package com.epam.spring.client.manager;

import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConfigPropertyManager {
	
	static Logger logger = LogManager.getLogger(ConfigPropertyManager.class);
	
	@Autowired
	private ResourceBundle resourceBundle;
	
	public String getProperty(String key) {
		logger.debug("ConfigPropertyManager: " + resourceBundle.getString(key));
		return resourceBundle.getString(key);
	}
}
