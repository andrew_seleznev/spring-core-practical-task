package com.epam.spring.client.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.spring.client.utils.ControllerResponse;

public interface ActionCommand {
	
	ControllerResponse execute(HttpServletRequest request);
	
}
