package com.epam.spring.client.command.impl;

import static com.epam.spring.client.constant.ConstantClient.*;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.MessageManager;
import com.epam.spring.client.utils.ControllerResponse;

@Component("changeLocaleCommand")
public class ChangeLocaleCommand implements ActionCommand {
	
	static Logger logger = LogManager.getLogger(ChangeLocaleCommand.class);
	
	private String currentPage;
	
	@Autowired
	private MessageManager messageManager;

	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		String lang = request.getParameter(ATTRIBUTE_LOCALE);
		request.getSession().setAttribute(ATTRIBUTE_LOCALE, lang);
		String page = (String) request.getSession().getAttribute(ATTRIBUTE_ACTUAL_PAGE);
		messageManager.setLocale(new Locale(String.valueOf(lang)));
		return new ControllerResponse(page, false);
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
	
}
