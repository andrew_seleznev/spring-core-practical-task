package com.epam.spring.client.command.impl;

import static com.epam.spring.client.constant.ConstantClient.*;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.utils.ControllerResponse;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;
import com.epam.spring.common.service.UserService;

@Component("viewAllTicketsCommand")
public class ViewAllTicketsCommand implements ActionCommand {
	
	static Logger logger = LogManager.getLogger(ViewAllTicketsCommand.class);
	
	@Autowired
	private ConfigPropertyManager configurationManager;
	@Autowired
	private UserService userService;

	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		if (checkUserRole(request)) {
			User user = (User) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
			Set<Ticket> tickets = userService.getAllUserTickets(user);
			request.getSession().setAttribute("userTickets", tickets);
			return new ControllerResponse(configurationManager.getProperty(PAGE_TICKETS), false);
		}
		request.getSession().invalidate();
		return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), true);
	}
	
	private boolean checkUserRole(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
		for (UserRole role : user.getUserRoles()) {
			if (role.equals(UserRole.RESGISTERED_USER)) {
				return true;
			}
		}
		return false;
	}

}
