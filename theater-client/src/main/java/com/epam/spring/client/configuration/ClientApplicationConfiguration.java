package com.epam.spring.client.configuration;

import java.util.ResourceBundle;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.epam.spring.common.config.AppConfig;

@Configuration
@ComponentScan("com.epam.spring.client")
@Import(AppConfig.class)
public class ClientApplicationConfiguration {
	
	private static final String PROPERTY_FILE_NAME = "config";

	@Bean
	public ResourceBundle resourceBundle() {
		return ResourceBundle.getBundle(PROPERTY_FILE_NAME);
	}
	
}
