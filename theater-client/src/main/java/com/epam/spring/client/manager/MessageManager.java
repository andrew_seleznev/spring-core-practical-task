package com.epam.spring.client.manager;

import static com.epam.spring.client.constant.ConstantClient.*;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class MessageManager {

	static Logger logger = LogManager.getLogger(MessageManager.class);
	private Locale locale = Locale.getDefault();
	private ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);

	public String getProperty(String key) {
		return resourceBundle.getString(key);
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);
	}

}
