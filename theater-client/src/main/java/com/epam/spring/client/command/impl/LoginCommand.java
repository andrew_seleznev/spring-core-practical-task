package com.epam.spring.client.command.impl;

import static com.epam.spring.client.constant.ConstantClient.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.manager.MessageManager;
import com.epam.spring.client.utils.ControllerResponse;
import com.epam.spring.client.validator.DataValidator;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.service.UserService;
import com.epam.spring.common.utils.UserPasswordEncoder;

@Component("loginCommand")
public class LoginCommand implements ActionCommand {

	static Logger logger = LogManager.getLogger(LoginCommand.class);

	@Autowired
	private ConfigPropertyManager configurationManager;
	@Autowired
	private MessageManager messageManager;
	@Autowired
	private UserService userService;
	@Autowired
	private UserPasswordEncoder userPasswordEncoder;

	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		if (dataValidate(request)) {
			String email = request.getParameter(PARAM_EMAIL);
			String password = request.getParameter(PARAM_PASSWORD);
			User user = userService.getUserByEmail(email);
			logger.debug("user test: " + user);
			if (user != null) {
				if (user.getPassword().equals(userPasswordEncoder.encode(password))) {
					request.getSession().setAttribute(ATTR_LOGGED_VISITOR, user);
					return new ControllerResponse(configurationManager.getProperty(PAGE_HOME), false);
				}
			}
		}
		request.setAttribute(ATTRIBUTE_ERROR_MSG, messageManager.getProperty(ERROR_MSG_LOGIN_FAILED));
		return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), false);
	}

	private boolean dataValidate(HttpServletRequest request) {
		if (DataValidator.EMAIL.check(request.getParameter(PARAM_EMAIL))
				&& DataValidator.PASSWORD.check(request.getParameter(PARAM_PASSWORD))) {
			return true;
		}
		return false;
	}

}
