package com.epam.spring.client.constant;

public class ConstantClient {
	
	public static final String PAGE_HOME = "path.page.home";
	public static final String PAGE_LOGIN = "path.page.login";
	public static final String PAGE_ERROR = "path.page.error";
	public static final String PAGE_EVENTS = "path.page.events";
	public static final String PAGE_BUY_TICKETS = "path.page.buy.tickets";
	public static final String PAGE_PAY_TICKETS = "path.page.pay.tickets";
	public static final String PAGE_TICKETS = "path.page.tickets";
	
	public static final String BUNDLE_NAME = "locale";
	
	public static final String ATTRIBUTE_ACTUAL_PAGE = "actualPage";
	public static final String ATTRIBUTE_ERROR_MSG = "errorMsg";
	public static final String ATTRIBUTE_LOCALE = "locale";
	public static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	public static final String ATTR_SESSION_DESTROYED = "sessionDestroyed";
	public static final String ATTRIBUTE_ERROR = "error";
	public static final String ATTRIBUTE_EVENTS = "events";
	
	public static final String PARAM_ACTION = "action";
	
	public static final String PARAM_FIRST_NAME = "first-name-param";
	public static final String PARAM_LAST_NAME = "last-name-param";
	public static final String PARAM_EMAIL = "email-param";
	public static final String PARAM_BIRTHDAY = "birthday-param";
	public static final String PARAM_PASSWORD = "password-param";
	
	public static final String PARAM_DATE_SELECT = "client-date-select-param";
	public static final String PARAM_SEATS_SELECT = "client-seats-select-param";
	
	public static final String ERROR_MSG_REGISTRATION_FAILED = "jsp.error.msg.registration.failed";
	public static final String ERROR_MSG_LOGIN_FAILED = "jsp.error.msg.login.failed";

}
