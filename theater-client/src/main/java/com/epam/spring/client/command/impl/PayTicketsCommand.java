package com.epam.spring.client.command.impl;

import static com.epam.spring.client.constant.ConstantClient.*;

import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.utils.ControllerResponse;
import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;
import com.epam.spring.common.service.BookingService;

@Component("payTicketsCommand")
public class PayTicketsCommand implements ActionCommand {
	
	static Logger logger = LogManager.getLogger(PayTicketsCommand.class);
	
	@Autowired
	private ConfigPropertyManager configurationManager;
	@Autowired
	private BookingService bookingService;
	
	@SuppressWarnings("unchecked")
	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		if (checkUserRole(request)) {
			Event event = (Event) request.getSession().getAttribute("selectedEvent");
			AirDate airDate = (AirDate) request.getSession().getAttribute("selectedAirDate");
			Set<Long> seats = (Set<Long>) request.getSession().getAttribute("selectedSeats");
			User user = (User) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
			Set<Ticket> ticketsToBook = seats.stream().map(seat -> new Ticket(user, event, airDate.getDateTime(), seat, airDate)).collect(Collectors.toSet());
	        bookingService.bookTickets(ticketsToBook);
	        request.getSession().removeAttribute("selectedEvent");
	        request.getSession().removeAttribute("selectedAirDate");
	        request.getSession().removeAttribute("selectedSeats");
			return new ControllerResponse(configurationManager.getProperty(PAGE_PAY_TICKETS), true);
		}
		request.getSession().invalidate();
		return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), true);
	}
	
	private boolean checkUserRole(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
		for (UserRole role : user.getUserRoles()) {
			if (role.equals(UserRole.RESGISTERED_USER)) {
				return true;
			}
		}
		return false;
	}

}
