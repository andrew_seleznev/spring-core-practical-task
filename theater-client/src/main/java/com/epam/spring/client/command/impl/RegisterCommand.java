package com.epam.spring.client.command.impl;
import static com.epam.spring.client.constant.ConstantClient.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.manager.MessageManager;
import com.epam.spring.client.utils.ControllerResponse;
import com.epam.spring.client.validator.DataValidator;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;
import com.epam.spring.common.service.UserService;
import com.epam.spring.common.utils.UserPasswordEncoder;

@Component("registerCommand")
public class RegisterCommand implements ActionCommand {
	
	static Logger logger = LogManager.getLogger(RegisterCommand.class);
	private final DateTimeFormatter formatter;
	
	public RegisterCommand() {
		formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	}
	
	@Autowired
	private ConfigPropertyManager configurationManager;
	@Autowired
	private MessageManager messageManager;
	@Autowired
	private UserService userService;
	@Autowired
	private UserPasswordEncoder userPasswordEncoder;
	
	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		if (dataValidate(request)) {
			String firstName = request.getParameter(PARAM_FIRST_NAME);
			String lastName = request.getParameter(PARAM_LAST_NAME);
			String email = request.getParameter(PARAM_EMAIL);
			LocalDate birthDay = LocalDate.parse(request.getParameter(PARAM_BIRTHDAY), formatter);
			String password = request.getParameter(PARAM_PASSWORD);
			User user = new User();
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setBirthDay(birthDay);
			user.setPassword(userPasswordEncoder.encode(password));
			Set<UserRole> roles = new HashSet<>();
			roles.add(UserRole.RESGISTERED_USER);
			user.setUserRoles(roles);
			logger.debug("user test: " + user);
			if (userService.getUserByEmail(email) != null) {
				request.setAttribute(ATTRIBUTE_ERROR_MSG, messageManager.getProperty(ERROR_MSG_REGISTRATION_FAILED));
				return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), false);
			}
			request.getSession().setAttribute(ATTR_LOGGED_VISITOR, user);
			userService.save(user);
		} else {
			request.setAttribute(ATTRIBUTE_ERROR_MSG, messageManager.getProperty(ERROR_MSG_REGISTRATION_FAILED));
			return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), false);
		}
		return new ControllerResponse(configurationManager.getProperty(PAGE_HOME), false);
	}
	
	private boolean dataValidate(HttpServletRequest request) {
		if (DataValidator.FIRST_NAME.check(request.getParameter(PARAM_FIRST_NAME)) &&
			DataValidator.LAST_NAME.check(request.getParameter(PARAM_LAST_NAME)) &&
			DataValidator.EMAIL.check(request.getParameter(PARAM_EMAIL)) &&
			DataValidator.DATA.check(request.getParameter(PARAM_BIRTHDAY)) &&
			DataValidator.PASSWORD.check(request.getParameter(PARAM_PASSWORD))) {
			LocalDate date = LocalDate.parse(request.getParameter(PARAM_BIRTHDAY), formatter);
			if (date.isBefore(LocalDate.now()) && userService.getUserByEmail(request.getParameter(PARAM_EMAIL)) == null) {
				return true;
			}
		};
		return false;
	}

}
