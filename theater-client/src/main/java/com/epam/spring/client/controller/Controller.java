package com.epam.spring.client.controller;

import java.io.IOException;
import static com.epam.spring.client.constant.ConstantClient.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.configuration.ClientApplicationConfiguration;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.utils.ControllerResponse;
import com.epam.spring.common.config.AppConfig;

@WebServlet("/Controller")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = -6654253971558325813L;
	static Logger logger = LogManager.getLogger(Controller.class);
	private ApplicationContext context;
	
	@Override
	public void init() throws ServletException {
		super.init();
		context = new AnnotationConfigApplicationContext(ClientApplicationConfiguration.class, AppConfig.class);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		ControllerResponse resp = null;
		String page = null;
		ActionCommand action = context.getBean(request.getParameter(PARAM_ACTION), ActionCommand.class);
		ConfigPropertyManager configurationManager = context.getBean("configPropertyManager", ConfigPropertyManager.class);
		try {
			resp = action.execute(request);
			page = resp.getPage();
			request.getSession(true).setAttribute(ATTRIBUTE_ACTUAL_PAGE, page);
		} catch (Exception e) {
			logger.error(e);
			request.setAttribute(ATTRIBUTE_ERROR, e.getMessage());
			resp = new ControllerResponse(configurationManager.getProperty(PAGE_ERROR), false);
		}
		if (resp.isRedirect()) {
			response.sendRedirect(request.getContextPath() + page);
		} else {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			dispatcher.forward(request, response);
		}
	}
}
