package com.epam.spring.client.command.impl;

import static com.epam.spring.client.constant.ConstantClient.ATTRIBUTE_ACTUAL_PAGE;
import static com.epam.spring.client.constant.ConstantClient.PAGE_LOGIN;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.utils.ControllerResponse;

@Component("viewLoginPage")
public class InitCommand implements ActionCommand {

	static Logger logger = LogManager.getLogger(LoginCommand.class);

	@Autowired
	private ConfigPropertyManager configurationManager;

	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		request.getSession().setAttribute(ATTRIBUTE_ACTUAL_PAGE, PAGE_LOGIN);
		return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), false);
	}

}
