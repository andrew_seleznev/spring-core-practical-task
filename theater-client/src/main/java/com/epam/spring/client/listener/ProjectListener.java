package com.epam.spring.client.listener;
import static com.epam.spring.client.constant.ConstantClient.*;

import java.util.Locale;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class ProjectListener implements HttpSessionListener {
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		se.getSession().setAttribute(ATTRIBUTE_LOCALE, Locale.getDefault());
		se.getSession().setAttribute(ATTRIBUTE_ACTUAL_PAGE, PAGE_LOGIN);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		se.getSession().getServletContext().setAttribute(ATTR_SESSION_DESTROYED, true);
	}

}
