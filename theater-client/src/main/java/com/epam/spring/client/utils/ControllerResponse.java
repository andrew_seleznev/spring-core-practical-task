package com.epam.spring.client.utils;

public class ControllerResponse {
	
	private String page;
	private boolean isRedirect;
	
	public ControllerResponse(String page, boolean isRedirect) {
		this.page = page;
		this.isRedirect = isRedirect;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public boolean isRedirect() {
		return isRedirect;
	}

	public void setRedirect(boolean isRedirect) {
		this.isRedirect = isRedirect;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ControllerResponse [page=").append(page).append(", isRedirect=").append(isRedirect).append("]");
		return builder.toString();
	}
	
}
