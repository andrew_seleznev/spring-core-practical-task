package com.epam.spring.client.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum DataValidator {
	
	FIRST_NAME ("^[А-ЯЁA-Z][а-яА-ЯёЁa-zA-Z]{1,20}$"),
	LAST_NAME ("^[А-ЯЁA-Z][а-яА-ЯёЁa-zA-Z]{1,20}$"),
	EMAIL ("^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*"
			+ "@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\\.)*"
			+ "(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$"),
	DATA ("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)"
			+ "?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))"
			+ "$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$"),
	DATA_TIME ("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)"
			+ "?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))"
			+ "$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2}) ([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"),
	PASSWORD ("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).{4,20}$");
	private Pattern pattern;
	
	static Logger logger = LogManager.getLogger(DataValidator.class);
	
	private DataValidator(String regEx) {
		this.pattern = Pattern.compile(regEx);
	}
	
	public boolean check(String data) {
		boolean value = false;
		if (data != null) {
			Matcher matcher = this.pattern.matcher(data);
			if (matcher.matches()) {
				value = true;
			} else {
				logger.info("Validaton failed: " + this + " - '" + data + "'");
			}
		} else {
			logger.info("Validaton failed: " + this + " - '" + data + "'");
		}
		return value;
	}
}
