package com.epam.spring.client.command.impl;

import static com.epam.spring.client.constant.ConstantClient.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.utils.ControllerResponse;

@Component("logoutCommand")
public class LogoutCommand implements ActionCommand {
	
	@Autowired
	private ConfigPropertyManager configurationManager;

	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		request.getSession().invalidate();
		return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), true);
	}

}
