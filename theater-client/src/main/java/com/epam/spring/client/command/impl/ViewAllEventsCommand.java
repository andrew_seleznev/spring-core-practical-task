package com.epam.spring.client.command.impl;

import static com.epam.spring.client.constant.ConstantClient.*;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.utils.ControllerResponse;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;
import com.epam.spring.common.service.EventService;

@Component("viewAllEventsCommand")
public class ViewAllEventsCommand implements ActionCommand {
	
	@Autowired
	private ConfigPropertyManager configurationManager;
	@Autowired
	private EventService eventService;

	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		if (checkUserRole(request)) {
			Collection<Event> events = eventService.getAll();
			request.getSession().setAttribute(ATTRIBUTE_EVENTS, events);
			return new ControllerResponse(configurationManager.getProperty(PAGE_EVENTS), false);
		}
		request.getSession().invalidate();
		return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), true);
	}
	
	private boolean checkUserRole(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
		for(UserRole role : user.getUserRoles()) {
			if (role.equals(UserRole.RESGISTERED_USER)) {
				return true;
			}
		}
		return false;
	}

}
