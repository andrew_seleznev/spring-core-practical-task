package com.epam.spring.client.command.impl;

import static com.epam.spring.client.constant.ConstantClient.*;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.spring.client.command.ActionCommand;
import com.epam.spring.client.manager.ConfigPropertyManager;
import com.epam.spring.client.utils.ControllerResponse;
import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;
import com.epam.spring.common.service.AirDateService;
import com.epam.spring.common.service.BookingService;
import com.epam.spring.common.service.EventService;
import com.google.common.collect.Sets;

@Component("buyTicketsCommand")
public class BuyTicketsCommand implements ActionCommand {

	static Logger logger = LogManager.getLogger(BuyTicketsCommand.class);

	@Autowired
	private ConfigPropertyManager configurationManager;
	@Autowired
	private BookingService bookingService;
	@Autowired
	private AirDateService airDateService;
	@Autowired
	private EventService eventService;

	@Override
	public ControllerResponse execute(HttpServletRequest request) {
		if (checkUserRole(request)) {
			String date = request.getParameter("air-date-id");
			AirDate airDate = airDateService.getById(Long.parseLong(date));
			airDate.setEventId(Long.parseLong(request.getParameter("event-id")));
			Event event = eventService.getById(airDate.getEventId());
			User user = (User) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
			Set<Long> seats = parseStringToList(request.getParameter("seats"));
			double price = bookingService.getTicketsPrice(event, airDate.getDateTime(), user, seats);
			request.getSession().setAttribute("selectedEvent", event);
			request.getSession().setAttribute("selectedAirDate", airDate);
			request.getSession().setAttribute("selectedSeats", seats);
			request.getSession().setAttribute("selectedPrice", price);
			return new ControllerResponse(configurationManager.getProperty(PAGE_BUY_TICKETS), false);
		}
		request.getSession().invalidate();
		return new ControllerResponse(configurationManager.getProperty(PAGE_LOGIN), true);
	}

	private boolean checkUserRole(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
		for (UserRole role : user.getUserRoles()) {
			if (role.equals(UserRole.RESGISTERED_USER)) {
				return true;
			}
		}
		return false;
	}

	private Set<Long> parseStringToList(String param) {
		String[] items = param.replaceAll("\\s+","").replaceAll("\\[", "").replaceAll("\\]", "").split(",");
		Long[] results = new Long[items.length];
		for (int i = 0; i < items.length; i++) {
			try {
				results[i] = Long.parseLong(items[i]);
			} catch (NumberFormatException e) {
				logger.error(e);
			}
		}
		return Sets.newHashSet(results);
	}

}
