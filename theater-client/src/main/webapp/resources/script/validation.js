function doValidateRegistrationForm() {
	var res = true;
	var registerFirstNameId = '#register-first-name-id';
	var registerLastNameId = '#register-last-name-id';
	var registerEmailId = '#register-email-id'
	var registerBirthdayId = '#register-birthday-id';
	var registerPasswordId = '#register-password-id';
	
	var registerFirstNameField = '#first-name-error';
	var registerLastNameIdField = '#last-name-error';
	var registerEmailIdField = '#email-error'
	var registerBirthdayIdField = '#birthday-error';
	var registerPasswordIdField = '#password-error';

	resetError(registerFirstNameField, registerFirstNameId);
	resetError(registerLastNameIdField, registerLastNameId);
	resetError(registerEmailIdField, registerEmailId);
	resetError(registerBirthdayIdField, registerBirthdayId);
	resetError(registerPasswordIdField, registerPasswordId);
	
	if (!checkFirstName(registerFirstNameId)) {
		showError(registerFirstNameField, registerFirstNameId);
		res = false;
	}
	if (!checkLastNameId(registerLastNameId)) {
		showError(registerLastNameIdField, registerLastNameId);
		res = false;
	}
	if (!checkEmail(registerEmailId)) {
		showError(registerEmailIdField, registerEmailId);
		res = false;
	}
	if (!checkBirthday(registerBirthdayId)) {
		showError(registerBirthdayIdField, registerBirthdayId);
		res = false;
	}
	if (!checkPassword(registerPasswordId)) {
		showError(registerPasswordIdField, registerPasswordId);
		res = false;
	}
	return res;
}

function checkFirstName(input) {
	var regexp = /^[А-ЯЁA-Z][а-яА-ЯёЁa-zA-Z]{1,20}$/;
	return regexp.test($(input).val());
}

function checkLastNameId(input) {
	var regexp = /^[А-ЯЁA-Z][а-яА-ЯёЁa-zA-Z]{1,20}$/;
	return regexp.test($(input).val());
}

function checkEmail(input) {
	var regexp = /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/;
	return regexp.test($(input).val());
}

function checkBirthday(input) {
	var res = false;
	var regexp = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
	if (regexp.test($(input).val())) {
		var from = $(input).val().split(".");
		var f = new Date(from[2], from[1] - 1, from[0]);
		var now = new Date();
		if (f.setHours(0,0,0,0) < now.setHours(0,0,0,0)) {
			res = true;
		} else {
			res = false;
		}
	}
	return res;
}

function checkPassword(input) {
	var regexp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,20}$/;
	return regexp.test($(input).val());
}

function showError(field, input) {
	$(field).addClass('visible-error-message').removeClass('hidden-error-message');
	$(input).addClass('error-input');
}

function resetError(field, input) {
	$(field).addClass('hidden-error-message').removeClass('visible-error-message');
	$(input).removeClass('error-input');
}