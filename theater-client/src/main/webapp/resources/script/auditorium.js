$(document).ready(function() {
	$('#admin-auditorium-table').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
		enableUpdateAuditoriumButton();
		enableUpdateAuditoriumSeatsButton();
		enableRemoveAuditoriumButton();
	});
	$('#admin-auditorium-table thead tr').click(function() {
		$('#admin-auditorium-table tbody tr').removeClass('highlight');
		disableUpdateAuditoriumButton();
		disableUpdateAuditoriumSeatsButton();
		disableRemoveAuditoriumButton();
	});
});

function disableUpdateAuditoriumButton() {
	$(function() {
		$("#update-auditorium-button").prop("disabled", true);
	});
}

function enableUpdateAuditoriumButton() {
	$(function() {
		$("#update-auditorium-button").prop("disabled", false);
	});
}

function disableUpdateAuditoriumSeatsButton() {
	$(function() {
		$("#update-auditorium-seats-button").prop("disabled", true);
	});
}

function enableUpdateAuditoriumSeatsButton() {
	$(function() {
		$("#update-auditorium-seats-button").prop("disabled", false);
	});
}

function disableRemoveAuditoriumButton() {
	$(function() {
		$("#remove-auditorium-button").prop("disabled", true);
	});
}

function enableRemoveAuditoriumButton() {
	$(function() {
		$("#remove-auditorium-button").prop("disabled", false);
	});
}

$(document).ready(
		function() {
			$('#admin-auditorium-table').on(
					'click',
					'tbody tr',
					function(event) {
						$('#auditoriumId').val($(this).data('auditoriumId'));
						$('#auditorium-name-id').val(
								$(this).data('auditoriumName'));
						$('#auditorium-number-seats-id').val(
								$(this).data('auditoriumNumberOfSeats'));
					});
		});

$(document).ready(
		function() {
			$("#update-auditorium-seats-button").click(function(event) {
				event.preventDefault();
				doAjax();
			});

			function doAjax() {

				var id = $('#auditoriumId').val();
				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");

				$.ajax({
					type : "GET",
					contentType : "application/json",
					url : "/theater-advanced/auditorium/" + id + "/update",
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseAuditoriumHandler(data);
						$('#auditoriums').addClass("hidden-div").removeClass(
								'visible-div');
						$('#vip-seats').addClass("visible-div");
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
		});

$(document).ready(
		function() {
			$("#update-auditorium-vip-seats-button").click(function(event) {
				event.preventDefault();
				doAjax();
			});

			function doAjax() {

				var id = $('#auditoriumId').val();
				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				
				var data = {}
				data["vipSeats"] = $("#vip-seats-id").val();

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "/theater-advanced/auditorium/" + id + "/vipseats/update",
					data : JSON.stringify(data),
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseAuditoriumUpdateHandler(data);
						$('#vip-seats').addClass("hidden-div").removeClass(
								'visible-div');
						$('#auditoriums').addClass("visible-div");
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
		});

function responseAuditoriumHandler(response) {
	$(function() {
		$('#auditorium-name-id').val(response.content.name);
		$('#number-seats-id').val(response.content.numberOfSeats);
		var trHTML = '';
		var seats = response.content.numberOfSeats;
		var seatsVIP = jQuery.makeArray(response.content.vipSeats);
		var i;
		trHTML += '<select multiple="multiple" id="vip-seats-id">';
		for (i = 1; i <= seats; i++) {
			if (jQuery.inArray(i, seatsVIP) != -1) {
				trHTML += '<option value=" ' + i + '"selected>' + i
						+ '</option>';
			} else {
				trHTML += '<option value=" ' + i + '">' + i + '</option>';
			}
		}
		trHTML += '</select>';
		$('#vip').html(trHTML);
		$('#vip-seats-id').multiSelect(
				{
					selectableHeader : "<div class='custom-header'>"
							+ $('#vip').data('availableSeats') + "</div>",
					selectionHeader : "<div class='custom-header'>"
							+ $('#vip').data('selectionSeats') + "</div>",
					afterSelect : function() {
						console.log("afterSelect", $('#vip-seats-id').val());
					},
					afterDeselect : function() {
						console.log("afterDeselect", $('#vip-seats-id').val());
					}
				});
	});
}

$(document).ready(function() {
	$("#update-auditorium-button").click(function(event) {
		event.preventDefault();
		doAjax();
	});

	function doAjax() {

		var id = $('#auditoriumId').val();
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");

		var data = {}
		data["name"] = $("#auditorium-name-id").val();
		data["numberOfSeats"] = $("#auditorium-number-seats-id").val();

		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "/theater-advanced/auditorium/" + id + "/update",
			data : JSON.stringify(data),
			dataType : 'json',
			timeout : 100000,
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success : function(data) {
				console.log("SUCCESS: ", data);
				responseAuditoriumUpdateHandler(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
});

$(document).ready(function() {
	$("#add-auditorium-button").click(function(event) {
		event.preventDefault();
		doAjax();
	});

	function doAjax() {

		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");

		var data = {}
		data["name"] = $("#auditorium-name-id").val();
		data["numberOfSeats"] = $("#auditorium-number-seats-id").val();

		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "/theater-advanced/auditorium/add",
			data : JSON.stringify(data),
			dataType : 'json',
			timeout : 100000,
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success : function(data) {
				console.log("SUCCESS: ", data);
				responseAuditoriumUpdateHandler(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
});

$(document).ready(function() {
	$("#remove-auditorium-button").click(function(event) {
		event.preventDefault();
		doAjax();
	});

	function doAjax() {

		var id = $('#auditoriumId').val();
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");

		$.ajax({
			type : "DELETE",
			contentType : "application/json",
			url : "/theater-advanced/auditorium/" + id + "/remove",
			dataType : 'json',
			timeout : 100000,
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success : function(data) {
				console.log("SUCCESS: ", data);
				responseAuditoriumUpdateHandler(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
});

function responseAuditoriumUpdateHandler(response) {
	$(function() {
		$('#admin-auditorium-table tbody tr').remove();
		var trHTML = '';
		$.each(response.content, function() {
			trHTML += '<tr data-auditorium-id="' + this.id
					+ '" data-auditorium-name="' + this.name
					+ '" data-auditorium-number-of-seats = "'
					+ this.numberOfSeats + '"><td>' + this.name
					+ '</td><td>' + this.numberOfSeats + '</td><td>'
					+ this.vipSeats + '</td></tr>';
		});
		$('#admin-auditorium-table tbody').append(trHTML);
		disableUpdateAuditoriumButton();
		disableUpdateAuditoriumSeatsButton();
		disableRemoveAuditoriumButton();
	});
}