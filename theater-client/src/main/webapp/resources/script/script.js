$(document).ready(function() {
	$.datetimepicker.setLocale(localeCode.substring(0, 2));
	$.datetimepicker.setDateFormatter({
		parseDate : function(date, format) {
			var d = moment(date, format);
			return d.isValid() ? d.toDate() : false;
		},

		formatDate : function(date, format) {
			return moment(date).format(format);
		}
	});
	$('#register-birthday-id').datetimepicker({
		timepicker : false,
		format : 'DD.MM.YYYY',
		formatDate : 'DD.MM.YYYY',
		maxDate : '0',
		step : 30
	});
});

$(document).ready(function() {
	$("#register-form-id").submit(function(event) {
		event.preventDefault();
		var valid = true;
		if (doValidateRegistrationForm()) {
			console.log('---> validation works fine');
			valid = true;
		} else {
			console.log('---> validation works bad');
			valid = false;
		}
		if (valid) this.submit();
	});
});

$(document).ready(function() {
	$('.client-event-item').click(function() {
		var currentId;
		var id = $(this).data('selectedEventId');
		$(this).siblings().removeClass('highlight');
		$('#client-event-item-' + id).addClass('highlight');
	});
});

var selectedEventId;

$('.client-event-auditoriums-select').click(function () {
     var optionSelected = $(this).find("option:selected");
     var valueSelected  = optionSelected.val();
     console.log('---> valueSelected: ' + valueSelected);
     var freeSeats = optionSelected.data('freeSeatsId');
     selectedEventId = optionSelected.data('selectedEventId');
     $('.vip').addClass('invisible');
     $('.button-regular').addClass('invisible');
     seatsHandler(freeSeats, selectedEventId);
     $('#vip-' + selectedEventId).removeClass('invisible').addClass('visible');
 });

function seatsHandler(freeSeats, id) {
	$(function() {
		var trHTML = '';
		var i;
		var seats = jQuery.makeArray(freeSeats);
		console.log("freeSeats: ", freeSeats.length);
		trHTML += '<select name="client-seats-select-param" multiple="multiple" id="vip-seats-id-' + id + '">';
		for (i = 0; i < freeSeats.length; i++) {
			trHTML += '<option value=" ' + freeSeats[i] + '">' + freeSeats[i] + '</option>';
		}
		trHTML += '</select>';
		$('#vip-' + id).html(trHTML);
		$('#vip-seats-id-' + id).multiSelect(
				{
					selectableHeader : "<div class='custom-header'>"
							+ $('#vip-' + id).data('availableSeats') + "</div>",
					selectionHeader : "<div class='custom-header'>"
							+ $('#vip-' + id).data('selectionSeats') + "</div>",
					afterSelect : function() {
						console.log("afterSelect", $('#vip-seats-id-' + id).val());
					},
					afterDeselect : function() {
						console.log("afterDeselect", $('#vip-seats-id-' + id).val());
					}
				});
	});
};

$(document).ready(function() {
	$(".vip").change(function(event) {
		var seats = $('#vip-seats-id-' + selectedEventId).val();
		var airDateId = $('#air-date-select-id-' + selectedEventId).val();
			if(seats == null) {
				$('#button-buy-tickets-' + selectedEventId).addClass('invisible');
			} else {
				$('#button-buy-tickets-' + selectedEventId).removeClass('invisible');
			}
		$('#seats-id').val(seats);
		$('#air-date-id').val(airDateId);
		$('#event-id').val(selectedEventId);
		console.log("air-date-id: ", airDateId);
	});
});