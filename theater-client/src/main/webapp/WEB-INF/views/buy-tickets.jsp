<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<fmt:setBundle basename="locale" />
<div class="content">
	<form id="select-tickets-id" method="post" action="Controller">
	<input name="action" type="hidden" value="payTicketsCommand" />
		<b><fmt:message key="jsp.event.name" /></b>: <c:out value="${ sessionScope.selectedEvent.name }" /><br>
		<b><fmt:message key="jsp.event.price" /></b>: <c:out value="${ sessionScope.selectedEvent.basePrice }" /><br>
		<b><fmt:message key="jsp.event.rating" /></b>: <c:out value="${ sessionScope.selectedEvent.rating }" /><br>
		
		<c:set value="${sessionScope.selectedAirDate.dateTime}" var="date" />
		<c:set var="cleanedDateTime" value="${fn:replace(date, 'T', ' ')}" />
		<joda:parseDateTime var="parsed" pattern="yyyy-MM-dd HH:mm" value="${cleanedDateTime}" />
		
		<b><fmt:message key="jsp.air.date.dateTime" /></b>: <joda:format value="${parsed}" style="SS" /><br>
		<b><fmt:message key="jsp.auditorium.name" /></b>: <c:out value="${ sessionScope.selectedAirDate.auditorium.name }" /><br>
		<b><fmt:message key="jsp.seats.list" /></b>: <c:out value="${ sessionScope.selectedSeats }" /><br>
		<p>Price is ${sessionScope.selectedPrice}</p>
		<div class="button-regular">
			<input type="submit" value="<fmt:message key="jsp.button.pay.tickets" />" />
		</div>
	</form>
</div>
