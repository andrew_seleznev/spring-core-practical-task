<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<fmt:setBundle basename="locale" />
<div class="content">
	<div class="login-register">
		<div id="login-panel-id" class="login-panel">
			<h1>Login panel</h1>
			<strong><fmt:message key="title.login" /></strong>
			<form id="login-form-id" method="post" action="<c:url value="/Controller"/>">
				<input name="action" type="hidden" value="loginCommand" />
				<table id="login-table-id">
					<tbody>
						<tr>
							<td>
								<label class="required-field"><fmt:message key="jsp.panel.login.email" /></label> 
							</td>
							<td>
								<input id="login-email-id" name="email-param" type="text"  autocomplete="on"/>
							</td>
							<td>
								<div id="login-email-error" class="error-message hidden-error-message">
									<fmt:message key="jsp.error.email"/>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label class="required-field"><fmt:message key="jsp.panel.login.password" /></label> 
							</td>
							<td>
								<input id="login-password-id" name="password-param" type="text"  autocomplete="on"/>
							</td>
							<td>
								<div id="login-password-error" class="error-message hidden-error-message">
									<fmt:message key="jsp.error.password"/>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<div>
					<input id="login-btn-id" type="submit" value="<fmt:message key="jsp.panel.login.submit" />" />
				</div>
			</form>
		</div>
		<div id="register-panel-id" class="register-panel">
			<h1>Register panel</h1>
			<strong><fmt:message key="title.login" /></strong>
			<form id="register-form-id" method="post" action="Controller">
				<input name="action" type="hidden" value="registerCommand" />
				<table id="register-table-id">
					<tbody>
						<tr>
							<td>
								<label class="required-field"><fmt:message key="jsp.panel.register.first.name" /></label> 
							</td>
							<td>
								<input id="register-first-name-id" name="first-name-param" type="text"  autocomplete="on"/>
							</td>
							<td>
								<div id="first-name-error" class="error-message hidden-error-message">
									<fmt:message key="jsp.error.first.name"/>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label class="required-field"><fmt:message key="jsp.panel.register.last.name" /></label> 
							</td>
							<td>
								<input id="register-last-name-id" name="last-name-param" type="text"  autocomplete="on"/>
							</td>
							<td>
								<div id="last-name-error" class="error-message hidden-error-message">
									<fmt:message key="jsp.error.last.name"/>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label class="required-field"><fmt:message key="jsp.panel.register.email" /></label> 
							</td>
							<td>
								<input id="register-email-id" name="email-param" type="text"  autocomplete="on"/>
							</td>
							<td>
								<div id="email-error" class="error-message hidden-error-message">
									<fmt:message key="jsp.error.email"/>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label class="required-field"><fmt:message key="jsp.panel.register.birthday" /></label>  
							</td>
							<td>
								<input id="register-birthday-id" name="birthday-param" type="text"  autocomplete="on"/>
							</td>
							<td>
								<div id="birthday-error" class="error-message hidden-error-message">
									<fmt:message key="jsp.error.birthday"/>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label class="required-field"><fmt:message key="jsp.panel.register.password" /></label> 
							</td>
							<td>
								<input id="register-password-id" name="password-param" type="text"  autocomplete="on"/>
							</td>
							<td>
								<div id="password-error" class="error-message hidden-error-message">
									<fmt:message key="jsp.error.password"/>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<div>
					<input id="register-btn-id" type="submit" value="<fmt:message key="jsp.panel.register.submit" />" />
				</div>
			</form>
		</div>
	</div>
</div>
