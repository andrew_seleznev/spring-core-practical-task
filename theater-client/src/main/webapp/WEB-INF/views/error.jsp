<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<fmt:setBundle basename="locale" />
<div class="content">
	<h1>Error page !</h1>
	<strong><fmt:message key="title.error" /></strong>
	${error}
</div>
