<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<fmt:setBundle basename="locale" />
<div class="content">
	<c:if test="${ sessionScope.userTickets.size() > 0 }">
	<c:forEach items="${sessionScope.userTickets}" var="ticket">
		<div data-selected-ticket-id=${ ticket.id } class="client-ticket-item" id="client-ticket-item-${ticket.id}">
			<div style="display: inline-block; float: left; padding: 10px;">
				<img src="<c:url value="/resources/img/web-ticket.png"/>" alt="Ticket view" >
			</div>
			<div style="display: inline-block; float: left; padding: 10px;">
				<b><fmt:message key="jsp.event.name" /></b>: <c:out value="${ ticket.event.name }" /><br>
				<b><fmt:message key="jsp.event.price" /></b>: <c:out value="${ ticket.event.basePrice }" /><br>
				<b><fmt:message key="jsp.event.rating" /></b>: <c:out value="${ ticket.event.rating }" /><br>
				
				<c:set value="${ticket.dateTime}" var="date" />
				<c:set var="cleanedDateTime" value="${fn:replace(date, 'T', ' ')}" />
				<joda:parseDateTime var="parsed" pattern="yyyy-MM-dd HH:mm" value="${cleanedDateTime}" />
				
				<b><fmt:message key="jsp.air.date.dateTime" /></b>: <joda:format value="${parsed}" style="SS" /><br>
				<b><fmt:message key="jsp.auditorium.name" /></b>: <c:out value="${ ticket.airDate.auditorium.name }" /><br>
				<b><fmt:message key="jsp.seats.list" /></b>: <c:out value="${ ticket.seat }" /><br>
			</div>
		</div>
	</c:forEach>
	</c:if>
</div>
