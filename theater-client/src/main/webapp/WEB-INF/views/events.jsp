<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>

<fmt:setBundle basename="locale" />
<div class="content">
	<c:if test="${ sessionScope.events.size() > 0 }">
		<form id="select-tickets-id" method="post" action="Controller">
		<input name="action" type="hidden" value="buyTicketsCommand" />
		<input name="seats" id="seats-id" type="hidden" />
		<input name="air-date-id" id="air-date-id" type="hidden" />
		<input name="event-id" id="event-id" type="hidden" />
		<div id="client-event-list-id">
		<c:forEach items="${sessionScope.events}" var="event">
			<c:if test="${event.dates.size() > 0}">
			<div data-selected-event-id=${ event.id } class="client-event-item" id="client-event-item-${event.id}">
				<div style="display: inline-block; float: left; padding: 10px;">
					<img src="<c:url value="/resources/img/web-event.png"/>" alt="Event view" >
				</div>
				<div style="display: inline-block; float: left; padding: 10px;">
					<b><fmt:message key="jsp.event.name" /></b>: <c:out value="${ event.name }" /><br>
					<b><fmt:message key="jsp.event.price" /></b>: <c:out value="${ event.basePrice }" /><br>
					<b><fmt:message key="jsp.event.rating" /></b>: <c:out value="${ event.rating }" /><br>
					<select id="air-date-select-id-${event.id}" name="client-date-select-param" class="client-event-auditoriums-select">
						<c:forEach var="item" items="${event.dates}">
							<c:set value="${item.dateTime}" var="date" /> <c:set
								var="cleanedDateTime" value="${fn:replace(date, 'T', ' ')}" />
							<joda:parseDateTime var="parsed" pattern="yyyy-MM-dd HH:mm"
								value="${cleanedDateTime}" />
							<option data-free-seats-id="${ item.availableSeats }" data-selected-event-id=${ event.id } value="${item.id}">${item.auditorium.name} <joda:format value="${parsed}"
								style="SS" /></option>
						</c:forEach>
					</select><br><br>
					<div id="button-buy-tickets-${event.id}" class="invisible button-regular">
						<input type="submit" value="<fmt:message key="jsp.button.buy.tickets" />" />
					</div>
				</div>
				<div id="vip-${event.id}" class="vip" data-available-seats="<fmt:message key="jsp.available.seats" />" 
					data-selection-seats="<fmt:message key="jsp.selection.seats" />" style="float: left; padding: 10px;">
				</div>
			</div>
			</c:if>
		</c:forEach>
		</div>
		</form>
	</c:if>
</div>
