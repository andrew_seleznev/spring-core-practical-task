<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<fmt:setBundle basename="locale" />
<div class="sidebar">
	<div class="sidebar-item">
		<c:choose>
			<c:when test="${sideBarItem eq 'eventList' }">   
					&bull; <a style="font-weight: bold; color: #baff75;"
					href="<c:url value="/Controller"/>?action=viewAllEventsCommand"><fmt:message key="jsp.sidebar.events"/></a>
			</c:when>
			<c:otherwise>   
					&bull; <a href="<c:url value="/Controller"/>?action=viewAllEventsCommand"><fmt:message key="jsp.sidebar.events"/></a>
			</c:otherwise>
		</c:choose><br>
		<c:choose>
			<c:when test="${sideBarItem eq 'ticketList' }">   
					&bull; <a style="font-weight: bold; color: #baff75;"
					href="<c:url value="/Controller"/>?action=viewAllTicketsCommand"><fmt:message key="jsp.sidebar.tickets"/></a>
			</c:when>
			<c:otherwise>   
					&bull; <a href="<c:url value="/Controller"/>?action=viewAllTicketsCommand"><fmt:message key="jsp.sidebar.tickets"/></a>
			</c:otherwise>
		</c:choose>
	</div>
</div>