<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<fmt:setBundle basename="locale" />
<footer>
		&copy;
		<fmt:message key="footer.copyright" />
</footer>