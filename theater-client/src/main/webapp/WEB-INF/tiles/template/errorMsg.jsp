<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<fmt:setBundle basename="locale" />
<div class="header-error-msg">
	<c:if test="${ not empty errorMsg }">
		<fmt:message key="jsp.error.label" />: ${errorMsg}
	</c:if>
</div>