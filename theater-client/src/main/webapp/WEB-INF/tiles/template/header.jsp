<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<fmt:setBundle basename="locale" />
<header>
	<div class="header-error-msg">
		<c:if test="${ not empty errorRegMsg }"><fmt:message key="jsp.error.label" />: ${errorRegMsg}</c:if>
	</div>
	<div class="header-title">
		<div class="header-message">
			<strong><fmt:message key="title.client.main" /></strong>
		</div>
		<c:if test="${ not empty sessionScope.loggedVisitor }">
		<div class="header-logout button-logout">
			<form id="logout-form-id" action="Controller" method="post">
				<input name="action" type="hidden" value="logoutCommand" />
				<b>Welcome : </b>${sessionScope.loggedVisitor.email} |  
					<input type="submit" value="Logout"/>
			</form>
		</div>
		</c:if>
	</div>
	<div id="header-locale-id" class="header-locale">
		<a href="<c:url value="/Controller"/>?action=changeLocaleCommand&locale=en"><fmt:message key="title.en" /></a>
		<a href="<c:url value="/Controller"/>?action=changeLocaleCommand&locale=ru"><fmt:message key="title.ru" /></a>
	</div>
</header>