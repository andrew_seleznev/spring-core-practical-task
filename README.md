Skeleton project for Spring Core MOOC course on university.epam.com

Clone the repository using your EPAM's login and password.

The hometask assignment can be found on project [Wiki](https://git.epam.com/yuriy_tkach/spring-core-hometask-skeleton/wikis/home)

Link to git.epam.com (https://git.epam.com/andrei_seliazniou/spring-core-practical-task.git) 
Link to bitbucket.org (https://andrew_seleznev@bitbucket.org/andrew_seleznev/spring-core-practical-task.git)