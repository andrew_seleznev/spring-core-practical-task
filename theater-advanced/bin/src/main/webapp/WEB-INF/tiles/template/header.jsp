<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<header id="header">
	<div class="header">
		<h1>Header</h1>
		<div id="header-locale">
			<div id="locale_en">
				<a href="?locale=en_US"><fmt:message key="title.en" /></a>
			</div>
			<div id="locale_ru">
				<a href="?locale=ru_RU"><fmt:message key="title.ru" /></a>
			</div>
		</div>
	</div>
</header>