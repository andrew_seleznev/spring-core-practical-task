package com.epam.spring.advanced.utils;

public class AjaxResponseBody {
	
	Object content;
	
	String code;

	String msg;

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AjaxResponseBody [content=").append(content).append(", code=").append(code).append(", msg=")
				.append(msg).append("]");
		return builder.toString();
	}
	
}
