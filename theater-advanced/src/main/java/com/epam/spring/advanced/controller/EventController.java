package com.epam.spring.advanced.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epam.spring.advanced.enumeration.Attributes;
import com.epam.spring.advanced.enumeration.Views;
import com.epam.spring.advanced.utils.AjaxResponseBody;
import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Auditorium;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.EventRating;
import com.epam.spring.common.service.AirDateService;
import com.epam.spring.common.service.AuditoriumService;
import com.epam.spring.common.service.EventService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class EventController {
	
	static Logger logger = LogManager.getLogger(EventController.class);
	private static final String ATTRIBUTE_EVENT_RATING = "eventRating";
	private static final String ATTRIBUTE_AUDITORIUMS = "auditoriums";
	
	private ObjectMapper mapper;
	private DateTimeFormatter formatter;

	
	public EventController() {
		mapper = new ObjectMapper();
		formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
	}
	
	@Autowired
	private EventService eventService;
	@Autowired
	private AuditoriumService auditoriumService;
	@Autowired
	private AirDateService airDateService;

	@RequestMapping(value = "/event/allevents", method = RequestMethod.GET)
	public ModelAndView viewAllEvents() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(Attributes.ATTRIBUTE_SIDE_BAR_ITEM.getAttributeName(),
				Attributes.ATTRIBUTE_EVENT_LIST.getAttributeName());
		modelAndView.addObject(Attributes.ATTRIBUTE_EVENT_LIST.getAttributeName(), eventService.getAll());
		modelAndView.addObject(ATTRIBUTE_EVENT_RATING, Arrays.asList(EventRating.values()));
		modelAndView.addObject(ATTRIBUTE_AUDITORIUMS, auditoriumService.getAll());
		modelAndView.setViewName(Views.VIEW_EVENTS_LIST.getViewName());
		return modelAndView;
	}
	
	@RequestMapping(value = "/event/{id}/update", method = RequestMethod.GET)
	public @ResponseBody Event updateEventAirdates(@PathVariable("id") long id, Model model) throws JsonProcessingException {
		return eventService.getById(id);
	}
	
	@RequestMapping(value="/event/{id}/update", method = RequestMethod.POST)
	public @ResponseBody Event updateEvent(@PathVariable("id") long id, @RequestBody String requestBody) throws JsonProcessingException, IOException {	
		JsonNode jsonNode = mapper.readTree(requestBody);
		String name = mapper.convertValue(jsonNode.get("name"), String.class);
		EventRating rating = mapper.convertValue(jsonNode.get("rating"), EventRating.class);
		Double basePrice = mapper.convertValue(jsonNode.get("basePrice"), Double.class);
		Event event = new Event();
		event.setId(id);
		event.setName(name);
		event.setRating(rating);
		event.setBasePrice(basePrice);
		eventService.updateEvent(event);
		return eventService.getById(id);
	}
	
	@RequestMapping(value="/event/add", method = RequestMethod.POST)
	public @ResponseBody Collection<Event> addEvent(@RequestBody String requestBody) throws JsonProcessingException, IOException {
		JsonNode jsonNode = mapper.readTree(requestBody);
		String name = mapper.convertValue(jsonNode.get("name"), String.class);
		EventRating rating = mapper.convertValue(jsonNode.get("rating"), EventRating.class);
		Double basePrice = mapper.convertValue(jsonNode.get("basePrice"), Double.class);
		Event event = new Event();
		event.setName(name);
		event.setRating(rating);
		event.setBasePrice(basePrice);
		logger.debug("new event: " + event);
		eventService.save(event);
		return eventService.getAll();
	}
	
	@RequestMapping(value="/event/{id}/remove", method = RequestMethod.DELETE)
	public @ResponseBody Collection<Event> removeEvent(@PathVariable("id") long id) {
		Event event = new Event();
		event.setId(id);
		logger.debug("remove event: " + event);
		eventService.remove(event);
		return eventService.getAll();
	}
	
	@RequestMapping(value="/event/{id}/datetime", method = RequestMethod.GET)
	public @ResponseBody AirDate getDateTime(@PathVariable("id") long id) {
		return airDateService.getById(id);
	}
	
	@RequestMapping(value="/event/{id}/datetime/update", method = RequestMethod.POST)
	public @ResponseBody Event updateAirDate(@PathVariable("id") long id, @RequestBody String requestBody) throws JsonProcessingException, IOException {
		JsonNode jsonNode = mapper.readTree(requestBody);
		LocalDateTime dateTime = LocalDateTime.parse(mapper.convertValue(jsonNode.get("dateTime"), String.class), formatter);
		Long airDateId = mapper.convertValue(jsonNode.get("airDateId"), Long.class);
		Long auditoriumId = mapper.convertValue(jsonNode.get("auditoriumId"), Long.class);
		AirDate airDate = new AirDate();
		airDate.setId(airDateId);
		airDate.setDateTime(dateTime);
		Auditorium auditorium = new Auditorium();
		auditorium.setId(auditoriumId);
		airDate.setAuditorium(auditorium);
		airDateService.updateAirDate(airDate);
		return eventService.getById(id);
	}
	
	@RequestMapping(value="/event/{id}/datetime/add", method = RequestMethod.POST)
	public @ResponseBody Event addAirDate(@PathVariable("id") long id, @RequestBody String requestBody) throws JsonProcessingException, IOException {
		JsonNode jsonNode = mapper.readTree(requestBody);
		LocalDateTime dateTime = LocalDateTime.parse(mapper.convertValue(jsonNode.get("dateTime"), String.class), formatter);
		Long auditoriumId = mapper.convertValue(jsonNode.get("auditoriumId"), Long.class);
		AirDate airDate = new AirDate();
		airDate.setEventId(id);
		airDate.setDateTime(dateTime);
		Auditorium auditorium = new Auditorium();
		auditorium.setId(auditoriumId);
		airDate.setAuditorium(auditorium);
		airDateService.save(airDate);
		return eventService.getById(id);
	}
	
	@RequestMapping(value="/event/{id}/datetime/delete", method = RequestMethod.DELETE)
	public @ResponseBody Event removeAirDate(@PathVariable("id") long id, @RequestBody String requestBody) throws JsonProcessingException, IOException {
		logger.debug("req: " + requestBody + " id: " + id);
		JsonNode jsonNode = mapper.readTree(requestBody);
		Long airDateId = mapper.convertValue(jsonNode.get("airDateId"), Long.class);
		AirDate airDate = new AirDate();
		airDate.setId(airDateId);
		airDateService.remove(airDate);
		logger.debug("res new air date: " + airDate);
		return eventService.getById(id);
	}
	
	@RequestMapping(value="/event/test", method = RequestMethod.POST)
	public @ResponseBody Collection<Event> testResponse() {
		AjaxResponseBody ajaxResponseBody = new AjaxResponseBody();
		return eventService.getAll();
	}
	
}
