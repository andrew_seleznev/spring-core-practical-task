package com.epam.spring.advanced.security;

import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;
import com.google.common.collect.Sets;

@Component
public class SecurityUser extends User implements UserDetails {

	private static final long serialVersionUID = -6727757777448831895L;

	private static final String ROLE_PREFIX = "ROLE_";
	
	public SecurityUser() {
		super();
	}

	public SecurityUser(User user) {
		if (user != null) {
			this.setId(user.getId());
			this.setFirstName(user.getFirstName());
			this.setLastName(user.getLastName());
			this.setEmail(user.getEmail());
			this.setBirthDay(user.getBirthDay());
			this.setPassword(user.getPassword());
			this.setUserRoles(user.getUserRoles());
			this.setTickets(user.getTickets());
		}
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<UserRole> userRoles = getUserRoles();
		Set<GrantedAuthority> authorities = Sets.newHashSet();
		if (userRoles != null) {
			userRoles.forEach(role -> authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + role.name())));
		}
		return authorities;
	}

	@Override
	public String getUsername() {
		return getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
