package com.epam.spring.advanced.enumeration;

public enum Attributes {
	
	ATTRIBUTE_SIDE_BAR_ITEM("sideBarItem"), ATTRIBUTE_EVENT_LIST("eventList");
	
	private String attributeName;
	
	private Attributes(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeName() {
		return attributeName;
	}
	
}
