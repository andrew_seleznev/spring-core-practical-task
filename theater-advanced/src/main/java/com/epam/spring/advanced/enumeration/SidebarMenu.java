package com.epam.spring.advanced.enumeration;

public enum SidebarMenu {
	
	EVENT_ITEM("eventItem");
	
	private String itemName;
	
	private SidebarMenu(String itemName) {
		this.itemName = itemName;
	}

	public String getItemName() {
		return itemName;
	}

}
