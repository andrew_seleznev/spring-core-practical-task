package com.epam.spring.advanced.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.epam.spring.common.domain.User;
import com.epam.spring.common.service.UserService;
@Component
public class CustomUserDetailsService implements UserDetailsService {
	@Autowired
    private UserService userService;
	
	public CustomUserDetailsService() {
		System.out.println("CustomUserDetailsService init");
	}
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userService.getUserByEmail(userName);
        if (user == null) {
            throw new UsernameNotFoundException("User with Login " + userName + " not found");
        }
        SecurityUser securityUser = new SecurityUser(user);
        System.out.println(securityUser);
        System.out.println("authorities: " + securityUser.getAuthorities());
        return securityUser;
	}

}
