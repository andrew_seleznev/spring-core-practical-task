package com.epam.spring.advanced.enumeration;

public enum Views {
	
	VIEW_EVENTS_LIST("events-list"), VIEW_EVENT_UPDATE("event-update");
	
	private String viewName;
	
	private Views(String viewName) {
		this.viewName = viewName;
	}

	public String getViewName() {
		return viewName;
	}
	
}
