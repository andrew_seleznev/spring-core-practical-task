package com.epam.spring.advanced.controller;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epam.spring.advanced.utils.AjaxResponseBody;
import com.epam.spring.common.domain.Auditorium;
import com.epam.spring.common.service.AuditoriumService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class AuditoriumController {

	static Logger logger = LogManager.getLogger(AuditoriumController.class);
	private static final String ATTRIBUTE_AUDITORIUM_LIST = "auditoriumList";
	private static final String VIEW_AUDITORIUM_LIST = "auditoriums-list";
	private static final String ATTRIBUTE_SIDE_BAR_ITEM = "sideBarItem";

	private ObjectMapper mapper;

	public AuditoriumController() {
		mapper = new ObjectMapper();
	}

	@Autowired
	private AuditoriumService auditoriumService;

	@RequestMapping(value = "/auditorium/allauditoriums", method = RequestMethod.GET)
	public ModelAndView findAllAuditoriums() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(ATTRIBUTE_SIDE_BAR_ITEM, ATTRIBUTE_AUDITORIUM_LIST);
		modelAndView.addObject(ATTRIBUTE_AUDITORIUM_LIST, auditoriumService.getAll());
		modelAndView.setViewName(VIEW_AUDITORIUM_LIST);
		return modelAndView;
	}
	
	@RequestMapping(value = "/auditorium/{id}/update", method = RequestMethod.GET)
	public @ResponseBody AjaxResponseBody findAuditorium(@PathVariable("id") long id) throws JsonProcessingException {
		AjaxResponseBody responseBody = new AjaxResponseBody();
		responseBody.setContent(auditoriumService.getById(id));
		return responseBody;
	}
	
	@RequestMapping(value = "/auditorium/{id}/update", method = RequestMethod.POST)
	public @ResponseBody AjaxResponseBody updateAuditorium(@PathVariable("id") long id, @RequestBody String requestBody) throws IOException {
		JsonNode jsonNode = mapper.readTree(requestBody);
		String name = mapper.convertValue(jsonNode.get("name"), String.class);
		Long numberOfSeats = mapper.convertValue(jsonNode.get("numberOfSeats"), Long.class);
		Auditorium auditorium = new Auditorium();
		auditorium.setId(id);
		auditorium.setName(name);
		auditorium.setNumberOfSeats(numberOfSeats);
		auditoriumService.updateAuditorium(auditorium);
		AjaxResponseBody responseBody = new AjaxResponseBody();
		responseBody.setContent(auditoriumService.getAll());
		return responseBody;
	}
	
	@RequestMapping(value = "/auditorium/add", method = RequestMethod.POST)
	public @ResponseBody AjaxResponseBody updateEventAirdates(@RequestBody String requestBody) throws IOException {
		JsonNode jsonNode = mapper.readTree(requestBody);
		String name = mapper.convertValue(jsonNode.get("name"), String.class);
		Long numberOfSeats = mapper.convertValue(jsonNode.get("numberOfSeats"), Long.class);
		Auditorium auditorium = new Auditorium();
		auditorium.setName(name);
		auditorium.setNumberOfSeats(numberOfSeats);
		auditoriumService.save(auditorium);
		AjaxResponseBody responseBody = new AjaxResponseBody();
		responseBody.setContent(auditoriumService.getAll());
		return responseBody;
	}
	
	@RequestMapping(value = "/auditorium/{id}/remove", method = RequestMethod.DELETE)
	public @ResponseBody AjaxResponseBody removeAuditorium(@PathVariable("id") long id) throws IOException {
		Auditorium auditorium = new Auditorium();
		auditorium.setId(id);
		auditoriumService.remove(auditorium);
		AjaxResponseBody responseBody = new AjaxResponseBody();
		responseBody.setContent(auditoriumService.getAll());
		return responseBody;
	}
	
	@RequestMapping(value = "/auditorium/{id}/vipseats/update", method = RequestMethod.POST)
	public @ResponseBody AjaxResponseBody updateAuditoriumVipSeats(@PathVariable("id") long id, @RequestBody String requestBody) throws IOException {
		JsonNode jsonNode = mapper.readTree(requestBody);
		List<Long> vipSeats = mapper.convertValue(jsonNode.get("vipSeats"), new TypeReference<List<Long>>(){});
		auditoriumService.saveAuditoriumVipSeats(id, vipSeats);
		AjaxResponseBody responseBody = new AjaxResponseBody();
		responseBody.setContent(auditoriumService.getAll());
		return responseBody;
	}
}
