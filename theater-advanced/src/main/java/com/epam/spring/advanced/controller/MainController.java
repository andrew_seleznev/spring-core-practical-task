package com.epam.spring.advanced.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {
 
   @RequestMapping(value = { "/home" }, method = RequestMethod.GET)
   public ModelAndView welcomePage() {
	   System.out.println("MainController");
	   ModelAndView modelAndView = new ModelAndView();
	   modelAndView.setViewName("home");
       return modelAndView;
   }
}
