$(document).ready(function() {
	$('#admin-events-table').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
		enableRemoveEventButton();
	});
	$('#admin-events-table thead tr').click(function() {
		$('#admin-events-table tbody tr').removeClass('highlight');
		disableRemoveEventButton();
	});
});

$(document).ready(function() {
	$('#admin-event-air-dates-table').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
		var id = $(this).data('airDateId');
		$('#airDateId').val(id);
		enableUpAirDateButton();
	});
	$('#admin-event-air-dates-table thead tr').click(function() {
		$('#admin-event-air-dates-table tbody tr').removeClass('highlight');
		disableUpAirDateButton();
		clearUpAirDateValues();
	});
});

function disableUpAirDateButton() {
	$(function() {
		$("#update-event-airdate-button").prop("disabled", true);
		$("#delete-event-airdate-button").prop("disabled", true);
		$('#datetimepicker').datetimepicker("hide");
	});
}

function enableUpAirDateButton() {
	$(function() {
		$("#update-event-airdate-button").prop("disabled", false);
		$("#delete-event-airdate-button").prop("disabled", false);
		$('#datetimepicker').datetimepicker("show");
	});
}

function disableRemoveEventButton() {
	$(function() {
		$("#update-event-airdates-button").prop("disabled", true);
		$("#remove-event-button").prop("disabled", true);
	});
}

function enableRemoveEventButton() {
	$(function() {
		$("#update-event-airdates-button").prop("disabled", false);
		$("#remove-event-button").prop("disabled", false);
	});
}

function clearUpAirDateValues() {
	$(function() {
		$("#datetimepicker").val("");
	});
}

$(document).ready(function() {
	$.datetimepicker.setLocale(localeCode.substring(0, 2));
	$.datetimepicker.setDateFormatter({
		parseDate : function(date, format) {
			var d = moment(date, format);
			return d.isValid() ? d.toDate() : false;
		},

		formatDate : function(date, format) {
			return moment(date).format(format);
		}
	});
	$('#datetimepicker').datetimepicker({
		format : 'DD.MM.YYYY hh:mm',
		formatTime : 'hh:mm',
		formatDate : 'DD.MM.YYYY',
		minDate : '0',
		step: 30
	});
});

$(document).ready(
		function() {
			$("#update-event-button").click(function(event) {
				event.preventDefault();
				doAjax();

			});

			function doAjax() {

				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				var id = $("#eventId").val();

				var data = {}
				data["id"] = id;
				data["rating"] = $("#eventRatings").val();
				data["basePrice"] = $("#eventBasePrice").val();
				data["name"] = $("#event-name-id").val();

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "/theater-advanced/event/" + id + "/update",
					data : JSON.stringify(data),
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseHandler(data);
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
});



$(document).ready(function() {
	$('#admin-event-air-dates-table').on('click', 'tbody tr', function(event) {
	
				var id = $(this).data('airDateId');
				$('#airDateId').val(id);
				getAirDate(id);
				
		function getAirDate(id) {			
				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				$.ajax({
							type : "GET",
							contentType : "application/json",
							url : "/theater-advanced/event/" + id + "/datetime",
							dataType : 'json',
							timeout : 100000,
							beforeSend : function(xhr) {
								xhr.setRequestHeader(header, token);
							},
							success : function(data) {
								console.log("SUCCESS: ", data);
								var lastEditDate = new Date();
								lastEditDate.setFullYear(data.dateTime.year,
										data.dateTime.monthValue - 1,
										data.dateTime.dayOfMonth);
								lastEditDate.setHours(data.dateTime.hour);
								lastEditDate.setMinutes(data.dateTime.minute);
								var responseDate = moment(lastEditDate).format('DD.MM.YYYY hh:mm');
								$('#datetimepicker').val(responseDate);
							},
							error : function(e) {
								console.log("ERROR: ", e);
							},
							done : function(e) {
								console.log("DONE");
							}
						});
		}
	});
});

$(document).ready(
		function() {
			$("#update-event-airdate-button").click(function(event) {
				event.preventDefault();
				doAjax();

			});

			function doAjax() {

				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				var id = $("#eventId").val();

				var data = {}
				data["airDateId"] = $("#airDateId").val();
				data["dateTime"] = $("#datetimepicker").val();
				data["auditoriumId"] = $("#eventAuditoriums").val();

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "/theater-advanced/event/" + id + "/datetime/update",
					data : JSON.stringify(data),
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseHandler(data);
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
});

$(document).ready(
		function() {
			$("#add-event-airdate-button").click(function(event) {
				event.preventDefault();
				doAjax();

			});

			function doAjax() {

				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				var id = $("#eventId").val();

				var data = {}
				data["dateTime"] = $("#datetimepicker").val();
				data["auditoriumId"] = $("#eventAuditoriums").val();

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "/theater-advanced/event/" + id + "/datetime/add",
					data : JSON.stringify(data),
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseHandler(data);
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
});

$(document).ready(
		function() {
			$("#delete-event-airdate-button").click(function(event) {
				event.preventDefault();
				var choice = confirm("Do you want to delete?");
				if (choice) {
				    doAjax();
				}
			});

			function doAjax() {

				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				var id = $("#eventId").val();

				var data = {}
				data["airDateId"] = $("#airDateId").val();

				$.ajax({
					type : "DELETE",
					contentType : "application/json",
					url : "/theater-advanced/event/" + id + "/datetime/delete",
					data : JSON.stringify(data),
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseHandler(data);
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
});

$(document).ready(
		function() {
			$("#add-event-button").click(function(event) {
				event.preventDefault();
				doAjax();
			});

			function doAjax() {

				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				
				var data = {}
				data["name"] = $("#eventNameId").val();
				data["rating"] = $("#newEventRating").val();
				data["basePrice"] = $("#eventBasePriceId").val();

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "/theater-advanced/event/add",
					data : JSON.stringify(data),
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseEventHandler(data);
						disableRemoveEventButton();
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
});

$(document).ready(
		function() {
			$("#remove-event-button").click(function(event) {
				event.preventDefault();
				var choice = confirm("Do you want to delete?");
				if (choice) {
				    doAjax();
				}
			});

			function doAjax() {

				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				var id = $("#eventId").val();

				$.ajax({
					type : "DELETE",
					contentType : "application/json",
					url : "/theater-advanced/event/" + id + "/remove",
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseEventHandler(data);
						disableRemoveEventButton();
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
});

$(document).ready(function() {
	$('#admin-events-table').on('click', 'tbody tr', function(event) {
				var id = $(this).data('eventId');
				$('#eventId').val(id);
	});
});

$(document).ready(
		function() {
			$("#update-event-airdates-button").click(function(event) {
				event.preventDefault();
				doAjax();
			});

			function doAjax() {
				
				var id = $('#eventId').val();
				console.log("event id ---> " + id);
				
				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				
				var data = {}
				data["name"] = $("#eventNameId").val();
				data["basePrice"] = $("#eventBasePriceId").val();

				$.ajax({
					type : "GET",
					contentType : "application/json",
					url : "/theater-advanced/event/" + id + "/update",
					data : JSON.stringify(data),
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						responseHandler(data);
						$('#events').addClass("hidden-div").removeClass('visible-div');
						$('#airdates').addClass("visible-div");
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
});

function responseHandler(response) {
	$(function() {
		$('#admin-event-air-dates-table tbody tr').remove();
		var trHTML = '';
		$('#eventNameId').html(response.name);
		$.each(response.dates, function() {
			var lastEditDate = new Date();
			lastEditDate.setFullYear(this.dateTime.year,
					this.dateTime.monthValue - 1,
					this.dateTime.dayOfMonth);
			lastEditDate.setHours(this.dateTime.hour);
			lastEditDate.setMinutes(this.dateTime.minute);
			lastEditDate = lastEditDate.toLocaleString(localeCode
					.substring(0, 2), {
				year : 'numeric',
				month : 'long',
				day : 'numeric',
				hour : 'numeric',
				minute : 'numeric',
			});
			trHTML += '<tr data-air-date-id="' + this.id
			+ '"><td>' + lastEditDate + '</td><td>'
					+ response.basePrice + '</td><td>'
					+ response.rating + '</td><td>'
					+ this.auditorium.name
					+ '</td></tr>';
		})
		$('#admin-event-air-dates-table tbody').append(trHTML);
		$('#event-name-id').val(response.name);
		$('#eventBasePrice').val(response.basePrice);
		$('#eventId').val(response.id);
		console.log("event name ---> " + response.name);
		disableUpAirDateButton();
	});
}

function responseEventHandler(response) {
	$(function() {
		$('#admin-events-table tbody tr').remove();
		var trHTML = '';
		$.each(response, function() {
			trHTML += '<tr data-event-id ="' + this.id
			+ '"><td>' + this.name + '</td><td>'
					+ this.basePrice + '</td><td>'
					+ this.rating + '</td></tr>';
		})
		$('#admin-events-table tbody').append(trHTML);
	});
}

//////////////////////////////////////////////////////////////////////
$(document).ready(
	function() {
		$("#test-button").click(function(event) {
			if (doValidate()) {
				console.log('---> validation works fine');
			} else {
				console.log('---> validation works bad');
			}
		});
	});

$(document).ready(
		function() {
			$("#test-button").click(function(event) {
				event.preventDefault();
				doAjax();
			});

			function doAjax() {
				
				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "/theater-advanced/event/test",
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(xhr) {
						xhr.setRequestHeader(header, token);
					},
					success : function(data) {
						console.log("SUCCESS: ", data);
						if (!data.valid) {
							console.log()
						}
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
});