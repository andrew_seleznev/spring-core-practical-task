function doValidate() {
	var res = true;
	var eventNameField = '#event-name-error';
	var eventNameInput = '#eventNameId';
	resetError(eventNameField, eventNameInput);
	if (!checkName(eventNameInput)) {
		showError(eventNameField, eventNameInput);
		res = false;
	}
	return res;
}

function checkName(input) {
	var regexp = /^[\d]{7,16}$/;
	var data-regexp = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
	var data-time-regexp = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2}) ([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
	return regexp.test($(input).val());
}

function showError(field, input) {
	$(field).addClass('visible-error-message').removeClass('hidden-error-message');
	$(input).addClass('error-input');
}

function resetError(field, input) {
	$(field).addClass('hidden-error-message').removeClass('visible-error-message');
	$(input).removeClass('error-input');
}