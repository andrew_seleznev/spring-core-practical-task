<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<spring:url value="/home" var="urlHome" />
<header>
	<div class="header-title">
		<div class="header-message">
			<a href="${urlHome}"><strong><spring:message code="title.title" /></strong></a>
			<%-- <strong><spring:message code="title.title" /></strong> --%>
		</div>
		<security:authorize access="hasRole('ROLE_BOOKING_MANAGER')">
			<div class="header-logout">
				<form action="${pageContext.request.contextPath}/logout"
					method="POST">
					<b>Welcome : </b>${pageContext.request.userPrincipal.name} | <input
						type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					<input type="submit" value="Logout" />
				</form>
			</div>
		</security:authorize>
	</div>
	<div class="header-locale">
		<a href="?language=en_US"><spring:message code="title.en" /></a> <a
			href="?language=ru_RU"><spring:message code="title.ru" /></a>
	</div>

</header>