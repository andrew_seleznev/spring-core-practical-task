<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="sidebar">
	<div class="sidebar-item">
		<c:choose>
			<c:when test="${sideBarItem eq 'eventList' }">   
					&bull; <a style="font-weight: bold; color: #baff75;"
					href="<c:url value="/event/allevents" />"> <spring:message
						code="sidebar.event"></spring:message></a>
			</c:when>
			<c:otherwise>   
					&bull; <a href="<c:url value="/event/allevents" />"><spring:message
						code="sidebar.event"></spring:message></a>
			</c:otherwise>
		</c:choose><br>
		<c:choose>
			<c:when test="${sideBarItem eq 'auditoriumList' }">   
					&bull; <a style="font-weight: bold; color: #baff75;"
					href="<c:url value="/auditorium/allauditoriums" />"> <spring:message
						code="sidebar.auditorium"></spring:message></a>
			</c:when>
			<c:otherwise>   
					&bull; <a href="<c:url value="/auditorium/allauditoriums" />"><spring:message
						code="sidebar.auditorium"></spring:message></a>
			</c:otherwise>
		</c:choose>
	</div>
</div>