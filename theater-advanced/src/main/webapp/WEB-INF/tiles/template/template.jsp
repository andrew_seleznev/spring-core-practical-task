<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%-- The list attribute is first converted into a scripting variable; after 
			that it is iterated using the <c:forEach> tag. The compound attributes are 
			then rendered one after the other  --%>
<tiles:importAttribute name="title" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="${title}" /></title>
<sec:csrfMetaTags/>
<c:set var="localeCode" value="${pageContext.response.locale}" />
<script>var localeCode = '${localeCode}';</script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/jquery-ui.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/jquery.datetimepicker.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/multi-select.css">
</head>
<body>
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="sidebar" />
	<tiles:insertAttribute name="content" />
	<tiles:insertAttribute name="footer" />
</body>
<script src="<c:url value="/resources/script/jquery-2.2.4.min.js"/>"></script>
<script src="<c:url value="/resources/script/script.js"/>"></script>
<script src="<c:url value="/resources/script/auditorium.js"/>"></script>
<script src="<c:url value="/resources/script/validation.js"/>"></script>
<script src="<c:url value="/resources/script/jquery-ui.js"/>"></script>
<script src="<c:url value="/resources/script/jquery.datetimepicker.full.min.js"/>"></script>
<script src="<c:url value="/resources/script/moment-with-locales.js"/>"></script>
<script src="<c:url value="/resources/script/moment.js"/>"></script>
<script src="<c:url value="/resources/script/jquery.multi-select.js"/>"></script>
</html>