<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>

<div class="content">
<input id="eventId" type="hidden" readonly="readonly">
<div id="events" class="visible-div">
	<h1>Events page !</h1>
		<div class="required-field" style="display: inline-block;"><strong><spring:message code="jsp.event.name" />:</strong> 
			<input id="eventNameId" type="text" autocomplete="off"/></div>
			<div id="event-name-error" class="error-message hidden-error-message">
				<fmt:message key="jsp.error.wrong.data"/>
			</div><br>
		<strong><spring:message code="jsp.event.price" />:</strong> <input id="eventBasePriceId" type="text"/><br>
		<strong><spring:message code="jsp.event.rating" />:</strong>
		<select name="rating" id="newEventRating">
			<c:forEach var="item" items="${eventRating}">
				<option value="${item}">${item}</option>
			</c:forEach>
		</select>
	<button id="add-event-button">
		<spring:message code="jsp.event.actions.add" />
	</button><br>
	<span style="font-size: small;"><spring:message code="jsp.info.required.field" /></span>
	<hr>
	<c:if test="${ eventList.size() > 0 }">
		<h3><strong><spring:message code="title.events" /></strong></h3>
		<table id="admin-events-table" class="common-table">
			<thead>
				<tr>
					<th><spring:message code="jsp.event.name" /></th>
					<th><spring:message code="jsp.event.price" /></th>
					<th><spring:message code="jsp.event.rating" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${eventList}" var="event">
					<tr data-event-id = ${ event.id }>
						<td><c:out value="${ event.name }" /></td>
						<td><c:out value="${ event.basePrice }" /></td>
						<td><c:out value="${ event.rating }" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<hr>
	</c:if>
	<button id="update-event-airdates-button" disabled="disabled">
		<spring:message code="jsp.event.actions.update.airdates" />
	</button>
	<button id="remove-event-button" disabled="disabled">
		<spring:message code="jsp.event.actions.remove" />
	</button>
	<button id="test-button">
		Test button
	</button>
</div>
<div id="airdates" class="hidden-div">
	<div id="localization" data-event-date="<spring:message code="jsp.event.date"/>"
		data-event-price="<spring:message code="jsp.event.price"/>"
		data-event-rating="<spring:message code="jsp.event.rating"/>"
		 style="dislpay:none;"></div>
	<h1>Update event air date page !</h1>
	<input id="airDateId" type="hidden" readonly="readonly">
	<strong><spring:message code="title.update.event" /></strong>
	<spring:url value="/event/update" var="updateEventUrl" />
	<div id="feedback"></div>
	<strong><spring:message code="jsp.event.name" />:</strong> <input id="event-name-id" type="text" />
	<strong><spring:message code="jsp.event.rating" />:</strong>
		<select name="rating" id="eventRatings">
			<c:forEach var="item" items="${eventRating}">
				<option value="${item}">${item}</option>
			</c:forEach>
		</select> <strong><spring:message code="jsp.event.price" />:</strong>
		<input name="basePrice" id="eventBasePrice" type="text"
			value="${eventForm.basePrice}" />
		<button id="update-event-button">
			<spring:message code="jsp.event.actions.update" />
		</button>
	<form method="post" id="update-event-form">
		<input name="id" id="eventId" value="${eventForm.id}" type="hidden">
		<table id="admin-event-air-dates-table" class="common-table">
			<thead>
				<tr>
					<th><spring:message code="jsp.event.date" /></th>
					<th><spring:message code="jsp.event.price" /></th>
					<th><spring:message code="jsp.event.rating" /></th>
					<th><spring:message code="jsp.event.auditoriums" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${eventForm.dates}" var="airDate"
					varStatus="count">
					<tr data-air-date-id = ${airDate.id }>
						<td><c:set value="${airDate.dateTime}" var="date" /> <c:set
								var="cleanedDateTime" value="${fn:replace(date, 'T', ' ')}" />

							<joda:parseDateTime var="parsed" pattern="yyyy-MM-dd HH:mm"
								value="${cleanedDateTime}" /> <joda:format value="${parsed}"
								style="SS" /></td>
						<td><c:out value="${ eventForm.basePrice }" /></td>
						<td><c:out value="${ eventForm.rating }" /></td>
						<td><c:out value="${ airDate.auditorium.name }" /></td>
						<td style="display:none;">${ airDate.id }</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</form>
	<button id="update-event-airdate-button" disabled="disabled">
		<spring:message code="jsp.event.actions.update.airdate" />
	</button>
	<button id="delete-event-airdate-button" disabled="disabled">
		<spring:message code="jsp.event.actions.delete.airdate" />
	</button>
	<button id="add-event-airdate-button">
		<spring:message code="jsp.event.actions.add.airdate" />
	</button>
	<input id="datetimepicker" type="text">
	<select id="eventAuditoriums">
			<c:forEach var="item" items="${auditoriums}">
				<option value="${item.id}">${item.name}</option>
			</c:forEach>
	</select>
</div>
</div>