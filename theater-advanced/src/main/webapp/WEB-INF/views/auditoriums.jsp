<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>

<div class="content">
<input id="auditoriumId" type="hidden" readonly="readonly">
<div id="auditoriums" class="visible-div">
	<h1>Auditoriums page !</h1>
	<c:if test="${ auditoriumList.size() > 0 }">
		<h3><strong><spring:message code="title.auditoriums" /></strong></h3>
		<table id="admin-auditorium-table" class="common-table">
			<thead>
				<tr>
					<th><spring:message code="jsp.auditorium.name" /></th>
					<th><spring:message code="jsp.auditorium.number.seats" /></th>
					<th><spring:message code="jsp.auditorium.vip.seats" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${auditoriumList}" var="auditorium">
					<tr data-auditorium-id = "${ auditorium.id }" 
						data-auditorium-name = "${ auditorium.name }"
						data-auditorium-number-of-seats = "${ auditorium.numberOfSeats }">
						<td><c:out value="${ auditorium.name }" /></td>
						<td><c:out value="${ auditorium.numberOfSeats }" /></td>
						<td><c:out value="${ auditorium.vipSeats }" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<hr>
	</c:if>
	<strong><spring:message code="jsp.auditorium.name" />:</strong> <input id="auditorium-name-id" type="text"  autocomplete="off"/><br>
	<strong><spring:message code="jsp.auditorium.number.seats" />:</strong> <input id="auditorium-number-seats-id" type="text" autocomplete="off"/><br>
	<button id="update-auditorium-button" disabled="disabled">
		<spring:message code="jsp.auditorium.actions.update" />
	</button>
	<button id="update-auditorium-seats-button" disabled="disabled">
		<spring:message code="jsp.auditorium.actions.seats.update" />
	</button>
	<button id="add-auditorium-button">
		<spring:message code="jsp.auditorium.actions.add" />
	</button>
	<button id="remove-auditorium-button" disabled="disabled">
		<spring:message code="jsp.auditorium.actions.remove" />
	</button>
</div>
<div id="vip-seats" class="hidden-div">
	<h1>Update VIP page !</h1>
	<strong><spring:message code="jsp.auditorium.vip.seats" />:</strong>
		<div id="vip" data-available-seats="<spring:message code="jsp.auditorium.available.seats"/>" 
		data-selection-seats="<spring:message code="jsp.auditorium.selection.seats"/>"></div><br>
	<button id="update-auditorium-vip-seats-button">
		<spring:message code="jsp.auditorium.actions.seats.update" />
	</button>
</div>
</div>