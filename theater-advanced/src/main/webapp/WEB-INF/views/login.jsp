<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="content">
	<c:if test="${not empty error}">
		<div>
			<fmt:message key="${error}" />
		</div>
	</c:if>
	<c:if test="${not empty msg}">
		<div>
			<fmt:message key="${msg}" />
		</div>
	</c:if>
	<h1>Login</h1>
	<form name='loginForm'
		action="<c:url value='j_spring_security_check' />" method='POST'>
		<table>
			<tr>
				<td><fmt:message key="login.user" /></td>
				<td><input type='text' name='username'></td>
			</tr>
			<tr>
				<td><fmt:message key="login.password" /></td>
				<td><input type='password' name='password' /></td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="<fmt:message key="login.enter" />" /></td>
			</tr>
		</table>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
</div>