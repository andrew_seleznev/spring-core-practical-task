package com.epam.spring.common.dao.impl;

import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.spring.common.conf.AppTestConfig;
import com.epam.spring.common.domain.Auditorium;
import com.epam.spring.common.dao.AuditoriumDao;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppTestConfig.class })
@DbUnitConfiguration(databaseConnection = {"conn"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:auditorium-data.xml")
@DatabaseTearDown(value = "classpath:auditorium-data.xml", type = DatabaseOperation.DELETE_ALL)
public class AuditoriumDaoImplTest {

	@Autowired
	@Qualifier("auditoriumDao")
	private AuditoriumDao auditoriumDao;

	private final Long incorrectId = -1L;

	@Test
	public void saveTest() {
		Auditorium auditorium = new Auditorium();
		auditorium.setName("Minsk-arena");
		auditorium.setNumberOfSeats(1500L);
		Long id = auditoriumDao.save(auditorium);
		assertNotEquals(incorrectId, id);
	}

}
