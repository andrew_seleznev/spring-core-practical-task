package com.epam.spring.common.conf;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.epam.spring.common.config.AuditoriumsConfig;
import com.epam.spring.common.config.DiscountConfig;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;

@Configuration
@Import({ DiscountConfig.class, AuditoriumsConfig.class })
@ComponentScan({ "com.epam.spring.common.dao.impl", "com.epam.spring.common.service.impl", "com.epam.spring.common.aspect" })
@EnableAspectJAutoProxy
@PropertySource({ "classpath:oracle_jdbc_test.properties" })
public class AppTestConfig {

	@Value("${jdbc.driverClassName}")
	private String driverClassName;
	@Value("${jdbc.url}")
	private String url;
	@Value("${jdbc.username}")
	private String username;
	@Value("${jdbc.password}")
	private String password;
	@Value("${jdbc.initialSize}")
	private int initialSize;
	@Value("${jdbc.maxIdle}")
	private int maxTotal;
	@Value("${jdbc.minIdle}")
	private int minIdle;
	@Value("${jdbc.maxWaitMillis}")
	private long maxWaitMillis;
	@Value("${jdbc.poolPreparedStatements}")
	private boolean poolingStatements;

	@Bean
	DataSource dataSource() throws SQLException {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		dataSource.setInitialSize(initialSize);
		dataSource.setMaxTotal(maxTotal);
		dataSource.setMinIdle(minIdle);
		dataSource.setMaxWaitMillis(maxWaitMillis);
		dataSource.setPoolPreparedStatements(poolingStatements);
		return dataSource;
	}

	@Bean
	DatabaseConfigBean dbConfigBean() {
		DatabaseConfigBean dbConfigBean = new DatabaseConfigBean();
		dbConfigBean.setDatatypeFactory(new OracleDataTypeFactory());
		dbConfigBean.setCaseSensitiveTableNames(false);
		return dbConfigBean;
	}

	@Bean(name = {"conn"})
	DatabaseDataSourceConnectionFactoryBean dbSourceConnectionFactoryBean() throws SQLException {
		DatabaseDataSourceConnectionFactoryBean dbSourceConnectionFactoryBean = new DatabaseDataSourceConnectionFactoryBean();
		dbSourceConnectionFactoryBean.setDatabaseConfig(dbConfigBean());
		dbSourceConnectionFactoryBean.setDataSource(dataSource());
//		dbSourceConnectionFactoryBean.setSchema("THEATRE");
		dbSourceConnectionFactoryBean.setSchema("EPAM");
		return dbSourceConnectionFactoryBean;
	}

	@Bean
	public JdbcTemplate jdbcTemplate() throws SQLException {
		return new JdbcTemplate(dataSource());
	}

	@Bean
	public DataSourceTransactionManager transactionManager() throws SQLException {
		return new DataSourceTransactionManager(dataSource());
	}

}
