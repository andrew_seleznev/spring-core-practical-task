package com.epam.spring.common.service.impl;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.spring.common.dao.EventDao;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.EventRating;
import com.epam.spring.common.service.AuditoriumService;
import com.epam.spring.common.service.EventService;

@Service("eventService")
public class EventServiceImpl implements EventService {

	static Logger logger = LogManager.getLogger(EventServiceImpl.class);

	@Autowired
	@Qualifier("eventDao")
	private EventDao eventDao;
	@Autowired
	@Qualifier("auditoriumService")
	private AuditoriumService auditoriumService;

	@Override
	@Transactional
	public Long save(Event event) {
		Long eventId = eventDao.save(event);
		return eventId;
	}

	@Override
	public void remove(Event object) {
		eventDao.remove(object);
	}

	@Override
	public Event getById(Long id) {
		Event event = eventDao.getById(id);
		event.setDates(auditoriumService.getAllEventAuditoriums(event.getId()));
		return event;
	}

	@Override
	public Collection<Event> getAll() {
		Collection<Event> events = eventDao.getAll();
		for (Event event : events) {
			event.setDates(auditoriumService.getAllEventAuditoriums(event.getId()));
		}
		return events;
	}

	@Override
	public Event getByName(String name) {
		Event event = eventDao.getByName(name);
		event.setDates(auditoriumService.getAllEventAuditoriums(event.getId()));
		return event;
	}

	@Override
	public Long saveEventRating(EventRating eventRating) {
		Long eventRatingId = eventDao.saveEventRating(eventRating);
		return eventRatingId;
	}

	@Override
	public void updateEvent(Event event) {
		eventDao.updateEvent(event);
	}

}
