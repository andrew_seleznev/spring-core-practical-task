package com.epam.spring.common.dao;

import javax.annotation.Nonnull;

import com.epam.spring.common.domain.AirDate;

public interface AirDateDao extends AbstractDomainObjectDao<AirDate> {
	
	void updateAirDate(@Nonnull AirDate airDate);
}
