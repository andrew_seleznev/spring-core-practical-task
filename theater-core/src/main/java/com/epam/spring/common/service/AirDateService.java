package com.epam.spring.common.service;

import com.epam.spring.common.domain.AirDate;

public interface AirDateService extends AbstractDomainObjectService<AirDate> {
	
	void updateAirDate(AirDate airDate);
}
