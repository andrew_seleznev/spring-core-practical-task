package com.epam.spring.common.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
@Component
public class UserPasswordEncoder implements PasswordEncoder {
	private static MessageDigest digester;

    static {
        try {
            digester = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
    	UserPasswordEncoder encoder = new UserPasswordEncoder();
		System.out.println("MD5 for sznv: " + encoder.encode("sznv"));
	}

	@Override
	public String encode(CharSequence rawPassword) {
		if (digester == null) {
			return rawPassword.toString();
		}
		digester.update(rawPassword.toString().getBytes());
		byte byteData[] = digester.digest();
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1) hexString.append("0");
			hexString.append(hex);
		}
		return hexString.toString();
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return encode(rawPassword).equals(encodedPassword);
	}
}
