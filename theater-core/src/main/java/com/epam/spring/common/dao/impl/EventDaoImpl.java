package com.epam.spring.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import java.util.NavigableSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.epam.spring.common.dao.EventDao;
import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.EventRating;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;

@Component("eventDao")
public class EventDaoImpl implements EventDao {

	private static final String SAVE_EVENT_RATING_TYPE_SQL = "INSERT INTO EVENT_RATING (EVENT_RATING_TYPE) VALUES (?)";
	private static final String FIND_EVENT_BY_ID_SQL = "SELECT EVENT_ID, EVENT_NAME, EVENT_BASE_PRICE, EVENT_RATING_TYPE FROM EVENT INNER JOIN EVENT_RATING ON EVENT.EVENT_RATING_ID = EVENT_RATING.EVENT_RATING_ID WHERE EVENT_ID = ?";
	private static final String FIND_EVENT_BY_NAME_SQL = "SELECT EVENT_ID, EVENT_NAME, EVENT_BASE_PRICE, EVENT_RATING_TYPE FROM EVENT INNER JOIN EVENT_RATING ON EVENT.EVENT_RATING_ID = EVENT_RATING.EVENT_RATING_ID WHERE EVENT_NAME = ?";
	private static final String FIND_ALL_EVENTS_SQL = "SELECT EVENT_ID, EVENT_NAME, EVENT_BASE_PRICE, EVENT_RATING_TYPE FROM EVENT INNER JOIN EVENT_RATING ON EVENT.EVENT_RATING_ID = EVENT_RATING.EVENT_RATING_ID";
	private static final String FIND_ALL_USER_TICKETS_SQL = "SELECT TICKET_ID, VISITOR_ID, EVENT_ID, TICKET_DATE, TICKET_SEAT, AIR_DATE_ID FROM TICKET WHERE VISITOR_ID = ?";
	private static final String SAVE_EVENT_SQL = "INSERT INTO EVENT (EVENT_NAME, EVENT_BASE_PRICE, EVENT_RATING_ID) VALUES (?, ?, (SELECT EVENT_RATING_ID FROM EVENT_RATING WHERE EVENT_RATING_TYPE = ?))";
	private static final String SAVE_EVENT_AUDITORIUM_SQL = "INSERT INTO EVENT_AUDITORIUM (EVENT_ID, AUDITORIUM_ID, EVENT_DATE) VALUES (?, ?, ?)";
	private static final String REMOVE_EVENT_SQL = "DELETE FROM EVENT WHERE EVENT_ID = ?";
	private static final String UPDATE_EVENT_SQL = "UPDATE EVENT SET EVENT_NAME = ?, EVENT_RATING_ID = (SELECT EVENT_RATING_ID FROM EVENT_RATING WHERE EVENT_RATING_TYPE = ?), EVENT_BASE_PRICE = ? WHERE EVENT_ID = ?";
	private static final String EVENT_RATING_ID_COLUMN = "EVENT_RATING_ID";
	private static final String EVENT_ID_COLUMN = "EVENT_ID";
	private static final String EVENT_AUDITORIUM_ID_COLUMN = "EVENT_AUDITORIUM_ID";

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public Long save(Event object) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { EVENT_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_EVENT_SQL, columns);
				preparedStatement.setString(1, object.getName());
				preparedStatement.setDouble(2, object.getBasePrice());
				preparedStatement.setString(3, object.getRating().name());
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void remove(Event object) {
		jdbcTemplate.update(REMOVE_EVENT_SQL, object.getId());
	}

	@Override
	public Event getById(Long id) {
		Event event = null;
		event = jdbcTemplate.queryForObject(FIND_EVENT_BY_ID_SQL, new Object[] { id }, new RowMapper<Event>() {
			@Override
			public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
				Event event = new Event();
				event.setId(rs.getLong(1));
				event.setName(rs.getString(2));
				event.setBasePrice(rs.getDouble(3));
				event.setRating(EventRating.valueOf(rs.getString(4)));
				return event;
			}
		});
		return event;
	}

	@Override
	public Collection<Event> getAll() {
		List<Event> events = null;
		events = jdbcTemplate.query(FIND_ALL_EVENTS_SQL, new RowMapper<Event>() {
			@Override
			public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
				Event event = new Event();
				event.setId(rs.getLong(1));
				event.setName(rs.getString(2));
				event.setBasePrice(rs.getDouble(3));
				event.setRating(EventRating.valueOf(rs.getString(4)));
				return event;
			}

		});
		return events;
	}

	@Override
	public Event getByName(String name) {
		Event event = null;
		event = jdbcTemplate.queryForObject(FIND_EVENT_BY_NAME_SQL, new Object[] { name }, new RowMapper<Event>() {
			@Override
			public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
				Event event = new Event();
				event.setId(rs.getLong(1));
				event.setName(rs.getString(2));
				event.setBasePrice(rs.getDouble(3));
				event.setRating(EventRating.valueOf(rs.getString(4)));
				return event;
			}
		});
		return event;
	}

	@Override
	public Long saveEventRating(EventRating eventRating) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { EVENT_RATING_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_EVENT_RATING_TYPE_SQL, columns);
				preparedStatement.setString(1, eventRating.name());
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	@Override
	public NavigableSet<Ticket> getAllUserTickets(User user) {
		NavigableSet<Ticket> tickets = new TreeSet<>();
		List<Ticket> list = jdbcTemplate.query(FIND_ALL_USER_TICKETS_SQL, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, user.getId());
			}
		}, new RowMapper<Ticket>() {
			@Override
			public Ticket mapRow(ResultSet rs, int rowNum) throws SQLException {
				Event event = getById(rs.getLong(3));
				LocalDateTime dateTime = rs.getTimestamp(4).toLocalDateTime();
				Long seat = rs.getLong(5);
				Long airDateId = rs.getLong(6);
				AirDate airDate = new AirDate();
				airDate.setId(airDateId);
				Ticket ticket = new Ticket(user, event, dateTime, seat, airDate);
				ticket.setId(rs.getLong(1));
				return ticket;
			}
		});
		tickets.addAll(list);
		return tickets;
	}

	@Override
	public Long saveEventAuditorium(Long eventId, Long auditoriumId, LocalDateTime eventDate) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { EVENT_AUDITORIUM_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_EVENT_AUDITORIUM_SQL, columns);
				preparedStatement.setLong(1, eventId);
				preparedStatement.setLong(2, auditoriumId);
				preparedStatement.setTimestamp(3, Timestamp.valueOf(eventDate));
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void updateEvent(Event event) {
		jdbcTemplate.update(UPDATE_EVENT_SQL, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement preparedStatement) throws SQLException {
				preparedStatement.setString(1, event.getName());
				preparedStatement.setString(2, event.getRating().name());
				preparedStatement.setDouble(3, event.getBasePrice());
				preparedStatement.setLong(4, event.getId());
			}
		});
	}

}
