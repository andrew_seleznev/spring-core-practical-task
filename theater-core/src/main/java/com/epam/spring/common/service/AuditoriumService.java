package com.epam.spring.common.service;

import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Auditorium;

/**
 * @author Yuriy_Tkach
 */
public interface AuditoriumService extends AbstractDomainObjectService<Auditorium> {

    /**
     * Getting all auditoriums from the system
     * 
     * @return set of all auditoriums
     */
    @Nonnull Set<Auditorium> getAll();
    
    @Nonnull Set<Auditorium> getInitAuditoriums();
    /**
     * Finding auditorium by name
     * 
     * @param name
     *            Name of the auditorium
     * @return found auditorium or <code>null</code>
     */
    @Nullable Auditorium getByName(@Nonnull String name);
    
    List<AirDate> getAllEventAuditoriums(Long eventId);
    
    void updateAuditorium(@Nonnull Auditorium auditorium);
    
    void removeAuditoriumVipSeats(Long auditoriumId);
    
    void saveAuditoriumVipSeats(Long auditoriumId, List<Long> vipSeats);
}
