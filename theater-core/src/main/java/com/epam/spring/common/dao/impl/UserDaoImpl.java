package com.epam.spring.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.epam.spring.common.dao.UserDao;
import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;
import com.google.common.collect.Sets;

@Component("userDao")
public class UserDaoImpl implements UserDao {
	
	static Logger logger = LogManager.getLogger(UserDaoImpl.class);

	private static final String SAVE_USER_SQL = "INSERT INTO VISITOR (VISITOR_FIRST_NAME, VISITOR_LAST_NAME, VISITOR_EMAIL, VISITOR_BIRTHDAY, VISITOR_PASSWORD) VALUES (?,?,?,?,?)";
	private static final String SAVE_VISITOR_ROLE_SQL = "INSERT INTO VISITOR_ROLE (VISITOR_ID, ROLE_ID) VALUES (?,?)";
	private static final String FIND_USER_BY_EMAIL_SQL = "SELECT VISITOR_ID, VISITOR_FIRST_NAME, VISITOR_LAST_NAME, VISITOR_EMAIL, VISITOR_BIRTHDAY, VISITOR_PASSWORD FROM VISITOR WHERE VISITOR_EMAIL = ?";
	private static final String FIND_USER_ROLES_SQL = "SELECT ROLE_NAME FROM ROLE INNER JOIN VISITOR_ROLE ON VISITOR_ROLE.ROLE_ID = ROLE.ROLE_ID WHERE VISITOR_ROLE.VISITOR_ID = ?";
	private static final String FIND_USER_BY_ID_SQL = "SELECT VISITOR_ID, VISITOR_FIRST_NAME, VISITOR_LAST_NAME, VISITOR_EMAIL, VISITOR_BIRTHDAY FROM VISITOR WHERE VISITOR_ID = ?";
	private static final String FIND_ALL_USERS_SQL = "SELECT VISITOR_ID, VISITOR_FIRST_NAME, VISITOR_LAST_NAME, VISITOR_EMAIL, VISITOR_BIRTHDAY FROM VISITOR";
	private static final String FIND_ALL_EVENT_TICKETS_SQL = "SELECT TICKET_ID, VISITOR_ID, EVENT_ID, TICKET_DATE, TICKET_SEAT, AIR_DATE_ID FROM TICKET WHERE EVENT_ID = ? AND TICKET_DATE = ?";
	private static final String FIND_ALL_USER_TICKETS_SQL = "SELECT TICKET_ID, EVENT_ID, TICKET_DATE, TICKET_SEAT, AIR_DATE_ID FROM TICKET WHERE VISITOR_ID = ?";
	private static final String REMOVE_USER_SQL = "DELETE FROM VISITOR WHERE VISITOR_ID = ?";
	private static final String VISITOR_ID_COLUMN = "VISITOR_ID";
	private static final String VISITOR_ROLE_ID_COLUMN = "VISITOR_ROLE_ID";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long save(User object) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { VISITOR_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_USER_SQL, columns);
				preparedStatement.setString(1, object.getFirstName());
				preparedStatement.setString(2, object.getLastName());
				preparedStatement.setString(3, object.getEmail());
				preparedStatement.setTimestamp(4, Timestamp.valueOf(object.getBirthDay().atStartOfDay()));
				preparedStatement.setString(5, object.getPassword());
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void remove(User object) {
		jdbcTemplate.update(REMOVE_USER_SQL, object.getId());
	}

	@Override
	public User getById(Long id) {
		User user = null;
		user = jdbcTemplate.queryForObject(FIND_USER_BY_ID_SQL, new Object[] { id }, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setId(rs.getLong(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setEmail(rs.getString(4));
				user.setBirthDay(rs.getTimestamp(5).toLocalDateTime().toLocalDate());
				return user;
			}
		});
		return user;
	}

	@Override
	public Collection<User> getAll() {
		List<User> users = null;
		users = jdbcTemplate.query(FIND_ALL_USERS_SQL, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setId(rs.getLong(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setEmail(rs.getString(4));
				user.setBirthDay(rs.getTimestamp(5).toLocalDateTime().toLocalDate());
				return user;
			}

		});
		return users;
	}

	@Override
	public User getUserByEmail(String email) {
		User user = null;
		user = jdbcTemplate.queryForObject(FIND_USER_BY_EMAIL_SQL, new Object[] { email }, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setId(rs.getLong(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setEmail(rs.getString(4));
				user.setBirthDay(rs.getTimestamp(5).toLocalDateTime().toLocalDate());
				user.setPassword(rs.getString(6));
				return user;
			}
		});
		return user;
	}

	@Override
	public Set<Ticket> getAllEventTickets(Event event, LocalDateTime dateTime) {
		Set<Ticket> tickets = new TreeSet<>();
		List<Ticket> list = jdbcTemplate.query(FIND_ALL_EVENT_TICKETS_SQL, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, event.getId());
				ps.setTimestamp(2, Timestamp.valueOf(dateTime));
			}
		}, new RowMapper<Ticket>() {
			@Override
			public Ticket mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = getById(rs.getLong(2));
				LocalDateTime dateTime = rs.getTimestamp(4).toLocalDateTime();
				Long seat = rs.getLong(5);
				Long airDateId = rs.getLong(6);
				AirDate airDate = new AirDate();
				airDate.setId(airDateId);
				Ticket ticket = new Ticket(user, event, dateTime, seat, airDate);
				ticket.setId(rs.getLong(1));
				return ticket;
			}
		});
		tickets.addAll(list);
		return tickets;
	}

	@Override
	public Set<UserRole> getUserRoles(User user) {
		List<UserRole> userRoles = jdbcTemplate.query(FIND_USER_ROLES_SQL, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, user.getId());
			}
		}, new RowMapper<UserRole>() {
			@Override
			public UserRole mapRow(ResultSet rs, int rowNum) throws SQLException {
				return UserRole.valueOf(rs.getString(1));
			}
		});
		return Sets.newHashSet(userRoles);
	}

	@Override
	public Long saveUserRole(Long userId, Long roleId) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { VISITOR_ROLE_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_VISITOR_ROLE_SQL, columns);
				preparedStatement.setLong(1, userId);
				preparedStatement.setLong(2, roleId);
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	@Override
	public Set<Ticket> getAllUserTickets(User user) {
		List<Ticket> list = jdbcTemplate.query(FIND_ALL_USER_TICKETS_SQL, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, user.getId());
			}
		}, new RowMapper<Ticket>() {
			@Override
			public Ticket mapRow(ResultSet rs, int rowNum) throws SQLException {
				LocalDateTime dateTime = rs.getTimestamp(3).toLocalDateTime();
				Long seat = rs.getLong(4);
				Event event = new Event();
				event.setId(rs.getLong(2));
				AirDate airDate = new AirDate();
				airDate.setId(rs.getLong(5));
				Ticket ticket = new Ticket(user, event, dateTime, seat, airDate);
				ticket.setId(rs.getLong(1));
				logger.debug("Ticket: " + ticket);
				return ticket;
			}
		});
		logger.debug("List<Ticket>: " + list);
		return Sets.newHashSet(list);
	}
//	SELECT TICKET_ID, EVENT_ID, TICKET_DATE, TICKET_SEAT, AIR_DATE_ID FROM TICKET WHERE VISITOR_ID = ?

}
