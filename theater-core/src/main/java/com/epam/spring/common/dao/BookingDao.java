package com.epam.spring.common.dao;

import java.util.Set;

import javax.annotation.Nonnull;

import com.epam.spring.common.domain.Ticket;

public interface BookingDao {
	
	/**
     * Books tickets in internal system. If user is not
     * <code>null</code> in a ticket then booked tickets are saved with it
     * 
     * @param tickets
     *            Set of tickets
     */
    void bookTickets(@Nonnull Set<Ticket> tickets);
    
}
