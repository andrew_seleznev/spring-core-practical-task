package com.epam.spring.common.service.impl;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.spring.common.dao.AuditoriumDao;
import com.epam.spring.common.dao.EventDao;
import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Auditorium;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.service.AuditoriumService;
import com.epam.spring.common.service.BookingService;

@Transactional
@Service("auditoriumService")
public class AuditoriumServiceImpl implements AuditoriumService {

	static Logger logger = LogManager.getLogger(AuditoriumServiceImpl.class);
	@Resource(name = "auditoriums")
	private Set<Auditorium> auditoriums;
	@Autowired
	@Qualifier("auditoriumDao")
	private AuditoriumDao auditoriumDao;
	@Autowired
	private BookingService bookingService;
	@Autowired
	@Qualifier("eventDao")
	private EventDao eventDao;

	@Override
	public Set<Auditorium> getAll() {
		Collection<Auditorium> auditoriums = null;
		Set<Auditorium> set = new HashSet<>();
		auditoriums = auditoriumDao.getAll();
		set.addAll(auditoriums);
		return set;
	}

	@Override
	public Auditorium getByName(String name) {
		Optional<Auditorium> auditorium = getAll().stream().filter(a -> (a.getName().equals(name))).findFirst();
		return (auditorium.isPresent()) ? auditorium.get() : null;
	}

	@Override
	public Long save(Auditorium object) {
		Long auditoriumId = auditoriumDao.save(object);
		/*for (Long vipSeat : object.getVipSeats()) {
			auditoriumDao.saveAuditoriumVipSeat(auditoriumId, vipSeat);
		}*/
		return auditoriumId;
	}

	@Override
	public void remove(Auditorium object) {
		auditoriumDao.remove(object);
	}

	@Override
	public Auditorium getById(Long id) {
		Auditorium auditorium = auditoriumDao.getById(id);
		return auditorium;
	}

	@Override
	public Set<Auditorium> getInitAuditoriums() {
		return auditoriums;
	}

	@Override
	public List<AirDate> getAllEventAuditoriums(Long eventId) {
		List<AirDate> airDates = auditoriumDao.getAllEventAuditoriums(eventId);
		for(AirDate airDate : airDates) {
			airDate.setAvailableSeats(findFreeSeats(airDate));
		}
		return airDates;
	}

	@Override
	public void updateAuditorium(Auditorium auditorium) {
		auditoriumDao.updateAuditorium(auditorium);
	}

	@Override
	public void removeAuditoriumVipSeats(Long auditoriumId) {
		auditoriumDao.removeAuditoriumVipSeats(auditoriumId);
	}

	@Override
	public void saveAuditoriumVipSeats(Long auditoriumId, List<Long> vipSeats) {
		removeAuditoriumVipSeats(auditoriumId);
		auditoriumDao.saveAuditoriumVipSeats(auditoriumId, vipSeats);
	}
	
	private List<Long> findFreeSeats(AirDate airDate) {
        Auditorium aud = airDate.getAuditorium();
        
        Event event = eventDao.getById(airDate.getEventId());
		/*event.setDates(getAllEventAuditoriums(event.getId()));*/
        
        LocalDateTime time = airDate.getDateTime();
        Set<Ticket> tickets = bookingService.getPurchasedTicketsForEvent(event, time);
        
        List<Long> bookedSeats = tickets.stream().map(t -> t.getSeat()).collect(Collectors.toList());
        List<Long> freeSeats = aud.getAllSeats().stream().filter(seat -> !bookedSeats.contains(seat))
                .collect(Collectors.toList());
        return freeSeats;
    }

}
