package com.epam.spring.common.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.epam.spring.common.discount.DiscountStrategy;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.service.DiscountService;

@Service("discountService")
public class DiscountServiceImpl implements DiscountService {
	
	static Logger logger = LogManager.getLogger(DiscountServiceImpl.class);
	@Resource(name = "discountStrategies")
	private List<DiscountStrategy> discountStrategies;

	@Override
	public byte getDiscount(User user, Event event, LocalDateTime airDateTime,
			long numberOfTickets) {
		List<Byte> discountList = new ArrayList<>();
		for (DiscountStrategy strategy : discountStrategies) {
			discountList.add(strategy.getDiscount(user, event, airDateTime.toLocalDate(), numberOfTickets));
		}
		return Collections.max(discountList);
	}

}
