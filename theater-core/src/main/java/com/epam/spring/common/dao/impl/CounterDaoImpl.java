package com.epam.spring.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.epam.spring.common.dao.CounterDao;
import com.epam.spring.common.domain.Counter;
import com.epam.spring.common.domain.CountingCriteria;

@Component("counterDao")
public class CounterDaoImpl implements CounterDao {

	private static final String SAVE_COUNTER_SQL = "INSERT INTO COUNTER (COUNTED_OBJECT_ID, COUNTING_CRITERIA_ID, COUNTER_VALUE) VALUES (?, (SELECT COUNTING_CRITERIA_ID FROM COUNTING_CRITERIA WHERE COUNTING_CRITERIA_NAME = ?), ?)";
	private static final String FIND_COUNTER_SQL = "SELECT COUNTER_ID, COUNTED_OBJECT_ID, COUNTING_CRITERIA_NAME, COUNTER_VALUE FROM COUNTER INNER JOIN COUNTING_CRITERIA ON COUNTER.COUNTING_CRITERIA_ID = COUNTING_CRITERIA.COUNTING_CRITERIA_ID WHERE COUNTING_CRITERIA_NAME = ? AND COUNTED_OBJECT_ID = ?";
	private static final String FIND_COUNTER_BY_ID_SQL = "SELECT COUNTER_ID, COUNTED_OBJECT_ID, COUNTING_CRITERIA_NAME, COUNTER_VALUE FROM COUNTER INNER JOIN COUNTING_CRITERIA ON COUNTER.COUNTING_CRITERIA_ID = COUNTING_CRITERIA.COUNTING_CRITERIA_ID WHERE COUNTER_ID = ?";
	private static final String FIND_ALL_COUNTERS_SQL = "SELECT COUNTER_ID, COUNTED_OBJECT_ID, COUNTING_CRITERIA_NAME, COUNTER_VALUE FROM COUNTER INNER JOIN COUNTING_CRITERIA ON COUNTER.COUNTING_CRITERIA_ID = COUNTING_CRITERIA.COUNTING_CRITERIA_ID";
	private static final String UPDATE_COUNTER_SQL = "UPDATE COUNTER SET COUNTER_VALUE = ? WHERE COUNTING_CRITERIA_ID = (SELECT COUNTING_CRITERIA_ID FROM COUNTING_CRITERIA WHERE COUNTING_CRITERIA_NAME = ?) AND COUNTED_OBJECT_ID = ?";
	private static final String REMOVE_COUNTER_SQL = "DELETE FROM COUNTER WHERE COUNTER_ID = ?";
	private static final String COUNTER_ID_COLUMN = "COUNTER_ID";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long save(Counter object) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { COUNTER_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_COUNTER_SQL, columns);
				preparedStatement.setLong(1, object.getCountedObjectId());
				preparedStatement.setString(2, object.getCountingCriteria().name());
				preparedStatement.setLong(3, object.getCount());
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void remove(Counter object) {
		jdbcTemplate.update(REMOVE_COUNTER_SQL, object.getId());
	}

	@Override
	public Counter getById(Long id) {
		Counter counter = null;
		try {
			counter = jdbcTemplate.queryForObject(FIND_COUNTER_BY_ID_SQL, new Object[] { id },
					new RowMapper<Counter>() {
						@Override
						public Counter mapRow(ResultSet rs, int rowNum) throws SQLException {
							Counter counter = new Counter();
							counter.setId(rs.getLong(1));
							counter.setCountedObjectId(rs.getLong(2));
							counter.setCountingCriteria(CountingCriteria.valueOf(rs.getString(3)));
							counter.setCount(rs.getLong(4));
							return counter;
						}
					});
		} catch (EmptyResultDataAccessException e) {
			counter = null;
		}
		return counter;
	}

	@Override
	public Collection<Counter> getAll() {
		List<Counter> counters = null;
		try {
			counters = jdbcTemplate.query(FIND_ALL_COUNTERS_SQL, new RowMapper<Counter>() {
				@Override
				public Counter mapRow(ResultSet rs, int rowNum) throws SQLException {
					Counter counter = new Counter();
					counter.setId(rs.getLong(1));
					counter.setCountedObjectId(rs.getLong(2));
					counter.setCountingCriteria(CountingCriteria.valueOf(rs.getString(3)));
					counter.setCount(rs.getLong(4));
					return counter;
				}

			});
		} catch (EmptyResultDataAccessException e) {
			counters = null;
		}
		return counters;
	}

	@Override
	public Counter getByCountingCriteria(CountingCriteria countingCriteria, Long countedObjectId) {
		Counter counter = null;
		try {
			counter = jdbcTemplate.queryForObject(FIND_COUNTER_SQL,
					new Object[] { countingCriteria.name(), countedObjectId }, new RowMapper<Counter>() {
						@Override
						public Counter mapRow(ResultSet rs, int rowNum) throws SQLException {
							Counter counter = new Counter();
							counter.setId(rs.getLong(1));
							counter.setCountedObjectId(rs.getLong(2));
							counter.setCountingCriteria(CountingCriteria.valueOf(rs.getString(3)));
							counter.setCount(rs.getLong(4));
							return counter;
						}
					});
		} catch (EmptyResultDataAccessException e) {
			counter = null;
		}
		return counter;
	}

	@Override
	public void updateCounter(Counter counter) {
		jdbcTemplate.update(UPDATE_COUNTER_SQL, counter.getCount(), counter.getCountingCriteria().name(),
				counter.getCountedObjectId());
	}

}
