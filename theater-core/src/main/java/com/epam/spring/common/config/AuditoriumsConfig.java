package com.epam.spring.common.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.epam.spring.common.domain.Auditorium;

@Configuration
@PropertySource({ "classpath:auditoriums.properties",
		"classpath:price_calculation.properties" })
public class AuditoriumsConfig {

	@Value("${first.auditorium.name}")
	private String firstAuditoriumName;
	@Value("${first.auditorium.number.of.seats}")
	private long firstAuditoriumNumberOfSeats;
	@Value("#{'${first.auditorium.vip.seats}'.split(',')}")
	private Set<Long> firstAuditoriumVipSeats;

	@Value("${second.auditorium.name}")
	private String secondAuditoriumName;
	@Value("${second.auditorium.number.of.seats}")
	private long secondAuditoriumNumberOfSeats;
	@Value("#{'${second.auditorium.vip.seats}'.split(',')}")
	private Set<Long> secondAuditoriumVipSeats;

	@Value("${third.auditorium.name}")
	private String thirdAuditoriumName;
	@Value("${third.auditorium.number.of.seats}")
	private long thirdAuditoriumNumberOfSeats;
	@Value("#{'${third.auditorium.vip.seats}'.split(',')}")
	private Set<Long> thirdAuditoriumVipSeats;

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	@Scope("prototype")
	public Auditorium firstAuditorium() {
		Auditorium auditorium = new Auditorium();
		auditorium.setName(firstAuditoriumName);
		auditorium.setNumberOfSeats(firstAuditoriumNumberOfSeats);
		auditorium.setVipSeats(firstAuditoriumVipSeats);
		return auditorium;
	}

	@Bean
	@Scope("prototype")
	public Auditorium secondAuditorium() {
		Auditorium auditorium = new Auditorium();
		auditorium.setName(secondAuditoriumName);
		auditorium.setNumberOfSeats(secondAuditoriumNumberOfSeats);
		auditorium.setVipSeats(secondAuditoriumVipSeats);
		return auditorium;
	}

	@Bean
	@Scope("prototype")
	public Auditorium thirdAuditorium() {
		Auditorium auditorium = new Auditorium();
		auditorium.setName(thirdAuditoriumName);
		auditorium.setNumberOfSeats(thirdAuditoriumNumberOfSeats);
		auditorium.setVipSeats(thirdAuditoriumVipSeats);
		return auditorium;
	}

	@Bean
	public Set<Auditorium> auditoriums() {
		Set<Auditorium> auditoriums = new HashSet<>();
		auditoriums.add(firstAuditorium());
		auditoriums.add(secondAuditorium());
		auditoriums.add(thirdAuditorium());
		return auditoriums;
	}

}
