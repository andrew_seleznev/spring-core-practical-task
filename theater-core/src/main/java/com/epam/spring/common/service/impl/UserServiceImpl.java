package com.epam.spring.common.service.impl;

import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.spring.common.dao.EventDao;
import com.epam.spring.common.dao.UserDao;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;
import com.epam.spring.common.service.AirDateService;
import com.epam.spring.common.service.AuditoriumService;
import com.epam.spring.common.service.UserRoleService;
import com.epam.spring.common.service.UserService;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserService {

	static Logger logger = LogManager.getLogger(UserServiceImpl.class);
	@Autowired
	@Qualifier("userDao")
	private UserDao userDao;
	@Autowired
	@Qualifier("eventDao")
	private EventDao eventDao;
	@Autowired
	@Qualifier("auditoriumService")
	private AuditoriumService auditoriumService;
	@Autowired
	@Qualifier("userRoleService")
	private UserRoleService userRoleService;
	@Autowired
	private AirDateService airDateService;
	
	@Override
	public Long save(User object) {
		Long userId = userDao.save(object);
		for(UserRole role : object.getUserRoles()) {
			userDao.saveUserRole(userId, userRoleService.getByName(role.name()));
		}
		return userId;
	}

	@Override
	public void remove(User object) {
		userDao.remove(object);
	}

	@Override
	public User getById(Long id) {
		User user = userDao.getById(id);
		user.setTickets(eventDao.getAllUserTickets(user));
		for (Ticket ticket : user.getTickets()) {
			ticket.getEvent().setDates(auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()));
			/*ticket.setAirDate(airDateService.getById(ticket.getAirDate().getId()));*/
		}
		return user;
	}

	@Override
	public Collection<User> getAll() {
		Collection<User> users = userDao.getAll();
		for (User user : users) {
			user.setTickets(eventDao.getAllUserTickets(user));
			for (Ticket ticket : user.getTickets()) {
				ticket.getEvent().setDates(auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()));
				/*ticket.setAirDate(airDateService.getById(ticket.getAirDate().getId()));*/
			}
		}
		return users;
	}

	@Override
	public User getUserByEmail(String email) {
		User user = null;
		try {
			user = userDao.getUserByEmail(email);
			if (user != null) {
				user.setTickets(eventDao.getAllUserTickets(user));
				user.setUserRoles(userDao.getUserRoles(user));
				for (Ticket ticket : user.getTickets()) {
					ticket.getEvent().setDates(auditoriumService.getAllEventAuditoriums(ticket.getEvent().getId()));
					/*ticket.setAirDate(airDateService.getById(ticket.getAirDate().getId()));*/
				}
			}
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		return user;
	}

	@Override
	public Set<Ticket> getAllUserTickets(User user) {
		Set<Ticket> tickets = userDao.getAllUserTickets(user);
		for(Ticket ticket : tickets) {
			ticket.setEvent(eventDao.getById(ticket.getEvent().getId()));
			ticket.setAirDate(airDateService.getById(ticket.getAirDate().getId()));
		}
		return tickets;
	}

}
