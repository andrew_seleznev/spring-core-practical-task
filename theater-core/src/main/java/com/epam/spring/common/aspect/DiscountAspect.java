package com.epam.spring.common.aspect;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.epam.spring.common.discount.DiscountStrategy;
import com.epam.spring.common.domain.Counter;
import com.epam.spring.common.domain.CountingCriteria;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.service.CounterService;

@Aspect
@Component("discountAspect")
public class DiscountAspect {

	static Logger logger = LogManager.getLogger(DiscountAspect.class);

	@Autowired
	@Qualifier("counterService")
	private CounterService counterService;

	@Resource(name = "discountStrategies")
	private List<DiscountStrategy> discountStrategies;

	@Pointcut("execution(* com.epam.spring.common.service.impl.BookingServiceImpl.bookTickets(..))")
	private void bookEventTicketsMethod() {
	}

	@AfterReturning(pointcut = "bookEventTicketsMethod() && args(tickets)")
	public void countBookedEventTicket(Set<Ticket> tickets) {
		Ticket ticket = tickets.iterator().next();
		User user = ticket.getUser();
		Event event = ticket.getEvent();
		LocalDateTime airDateTime = ticket.getDateTime();

		Map<Class<?>, Byte> discountMap = new HashMap<>();
		for (DiscountStrategy strategy : discountStrategies) {
			discountMap.put(strategy.getClass(), strategy.getDiscount(user,
					event, airDateTime.toLocalDate(), tickets.size()));
		}
		Entry<Class<?>, Byte> maxEntry = findDiscountMaxEntry(discountMap);
		if (maxEntry.getValue() > 0) {
			updateTotalDiscountsCounter(maxEntry);
			if (user != null) {
				updateUsersDiscountsCounter(maxEntry, user);
			}
			logger.info(maxEntry.getKey().getSimpleName() + " was given "
					+ maxEntry.getValue() + " discount");
		}
	}

	private void updateTotalDiscountsCounter(Entry<Class<?>, Byte> maxEntry) {
		CountingCriteria countingCriteria = CountingCriteria.valueOf("TOTAL_"
				+ maxEntry.getKey().getSimpleName().toUpperCase());
		Counter counter = counterService.getByCountingCriteria(
				countingCriteria, 0L);
		if (counter == null) {
			Counter object = new Counter();
			object.setCountedObjectId(0);
			object.setCountingCriteria(countingCriteria);
			object.setCount(0);
			counterService.save(object);
			counter = counterService.getByCountingCriteria(
					countingCriteria, 0L);
		}
		counter.setCount(counter.getCount() + 1);
		counterService.updateCounter(counter);
		logger.info(maxEntry.getKey().getSimpleName() + " was given " + counterService.getByCountingCriteria(
				countingCriteria, 0L).getCount() + " time(s) TOTAL");
	}

	private void updateUsersDiscountsCounter(Entry<Class<?>, Byte> maxEntry,
			User user) {
		CountingCriteria countingCriteria = CountingCriteria.valueOf("USER_"
				+ maxEntry.getKey().getSimpleName().toUpperCase());
		Counter counter = counterService.getByCountingCriteria(
				countingCriteria, user.getId());
		if (counter == null) {
			Counter object = new Counter();
			object.setCountedObjectId(user.getId());
			object.setCountingCriteria(countingCriteria);
			object.setCount(0);
			counterService.save(object);
			counter = counterService.getByCountingCriteria(
					countingCriteria, user.getId());
		}
		counter.setCount(counter.getCount() + 1);
		counterService.updateCounter(counter);
		logger.info(maxEntry.getKey().getSimpleName() + " was given " + counterService.getByCountingCriteria(
				countingCriteria, user.getId()).getCount() + " time(s) for USER: " + user.getEmail());
	}

	private Entry<Class<?>, Byte> findDiscountMaxEntry(
			Map<Class<?>, Byte> discountMap) {
		Map.Entry<Class<?>, Byte> maxEntry = null;
		for (Map.Entry<Class<?>, Byte> entry : discountMap.entrySet()) {
			if (maxEntry == null
					|| entry.getValue().compareTo(maxEntry.getValue()) > 0) {
				maxEntry = entry;
			}
		}
		return maxEntry;
	}

}
