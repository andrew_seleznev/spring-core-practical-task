package com.epam.spring.common.service;

import java.util.Collection;

import javax.annotation.Nonnull;

import com.epam.spring.common.domain.UserRole;

public interface UserRoleService {
	
	/**
     * Saving new object to storage or updating existing one
     * 
     * @param object
     *            Object to save
     * @return saved object with assigned id
     */
    Long save(@Nonnull UserRole object);

    /**
     * Removing object from storage
     * 
     * @param object
     *            Object to remove
     */
    void remove(@Nonnull UserRole object);

    /**
     * Getting object by id from storage
     * 
     * @param id
     *            id of the object
     * @return Found object or <code>null</code>
     */
    Long getByName(String name);

    /**
     * Getting all objects from storage
     * 
     * @return collection of objects
     */
    @Nonnull Collection<UserRole> getAll();
    
}
