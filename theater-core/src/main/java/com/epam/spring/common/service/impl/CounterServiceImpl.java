package com.epam.spring.common.service.impl;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.epam.spring.common.dao.CounterDao;
import com.epam.spring.common.domain.Counter;
import com.epam.spring.common.domain.CountingCriteria;
import com.epam.spring.common.service.CounterService;

@Service("counterService")
public class CounterServiceImpl implements CounterService {

	static Logger logger = LogManager.getLogger(CounterServiceImpl.class);
	@Autowired
	@Qualifier("counterDao")
	private CounterDao counterDao;

	@Override
	public Long save(Counter object) {
		Long counterId = counterDao.save(object);
		return counterId;
	}

	@Override
	public void remove(Counter object) {
		counterDao.remove(object);
	}

	@Override
	public Counter getById(Long id) {
		Counter counter = counterDao.getById(id);
		return counter;
	}

	@Override
	public Collection<Counter> getAll() {
		Collection<Counter> counters = counterDao.getAll();
		return counters;
	}

	@Override
	public Counter getByCountingCriteria(CountingCriteria countingCriteria, Long countedObjectId) {
		Counter counter = counterDao.getByCountingCriteria(countingCriteria, countedObjectId);
		return counter;
	}

	@Override
	public void updateCounter(Counter counter) {
		counterDao.updateCounter(counter);
	}

}
