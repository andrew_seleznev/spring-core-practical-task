package com.epam.spring.common.config;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.epam.spring.common.discount.DiscountStrategy;

@Configuration
@ComponentScan("com.epam.spring.common.discount.impl")
public class DiscountConfig {
	
	@Resource(name = "birthDayStrategy")
	private DiscountStrategy birthDayStrategy;
	@Resource(name = "everyTenthTicketStrategy")
	private DiscountStrategy everyTenthTicketStrategy;
	
	@Bean
	public List<DiscountStrategy> discountStrategies() {
		List<DiscountStrategy> discountStrategies = new LinkedList<>();
		discountStrategies.add(birthDayStrategy);
		discountStrategies.add(everyTenthTicketStrategy);
		return discountStrategies;
	}
}
