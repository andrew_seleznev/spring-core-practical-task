package com.epam.spring.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.epam.spring.common.dao.AirDateDao;
import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Auditorium;

@Component("airDateDao")
public class AirDateDaoImpl implements AirDateDao {

	private static final String FIND_AIRDATE_BY_ID_SQL = "SELECT EVENT_DATE, AUDITORIUM_ID, EVENT_AUDITORIUM_ID, EVENT_ID FROM EVENT_AUDITORIUM WHERE EVENT_AUDITORIUM_ID = ?";
	private static final String UPDATE_AIRDATE_SQL = "UPDATE EVENT_AUDITORIUM SET AUDITORIUM_ID = ?, EVENT_DATE = ? WHERE EVENT_AUDITORIUM_ID = ?";
	private static final String SAVE_AIRDATE_SQL = "INSERT INTO EVENT_AUDITORIUM (EVENT_ID, AUDITORIUM_ID, EVENT_DATE) VALUES (?, ?, ?)";
	private static final String REMOVE_AIRDATE_SQL = "DELETE FROM EVENT_AUDITORIUM WHERE EVENT_AUDITORIUM_ID = ?";
	private static final String EVENT_AUDITORIUM_ID_COLUMN = "EVENT_AUDITORIUM_ID";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long save(AirDate object) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String[] columns = new String[] { EVENT_AUDITORIUM_ID_COLUMN };
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement(SAVE_AIRDATE_SQL, columns);
				preparedStatement.setLong(1, object.getEventId());
				preparedStatement.setLong(2, object.getAuditorium().getId());
				preparedStatement.setTimestamp(3, Timestamp.valueOf(object.getDateTime()));
				return preparedStatement;
			}
		}, keyHolder);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void remove(AirDate object) {
		jdbcTemplate.update(REMOVE_AIRDATE_SQL, object.getId());
	}

	@Override
	public AirDate getById(Long id) {
		AirDate airDate = null;
		airDate = jdbcTemplate.queryForObject(FIND_AIRDATE_BY_ID_SQL, new Object[] { id },
				new RowMapper<AirDate>() {
					@Override
					public AirDate mapRow(ResultSet rs, int rowNum) throws SQLException {
						AirDate airDate = new AirDate();
						airDate.setDateTime(rs.getTimestamp(1).toLocalDateTime());
						Auditorium auditorium = new Auditorium();
						auditorium.setId(rs.getLong(2));
						airDate.setAuditorium(auditorium);
						airDate.setId(rs.getLong(3));
						airDate.setEventId(rs.getLong(4));
						return airDate;
					}
				});
		return airDate;
	}

	@Override
	public Collection<AirDate> getAll() {
		return null;
	}

	@Override
	public void updateAirDate(AirDate airDate) {
		jdbcTemplate.update(UPDATE_AIRDATE_SQL, new PreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement preparedStatement) throws SQLException {
				preparedStatement.setLong(1, airDate.getAuditorium().getId());
				preparedStatement.setTimestamp(2, Timestamp.valueOf(airDate.getDateTime()));
				preparedStatement.setLong(3, airDate.getId());
			}
		});
	}

}
