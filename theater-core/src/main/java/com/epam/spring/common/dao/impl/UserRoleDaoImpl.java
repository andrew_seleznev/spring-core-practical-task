package com.epam.spring.common.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.epam.spring.common.dao.UserRoleDao;
import com.epam.spring.common.domain.UserRole;

@Component("userRoleDao")
public class UserRoleDaoImpl implements UserRoleDao {
	
	private static final String FIND_USER_ROLE_ID_BY_NAME_SQL = "SELECT ROLE_ID FROM ROLE WHERE ROLE_NAME = ?";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long save(UserRole object) {
		return null;
	}

	@Override
	public void remove(UserRole object) {
	}

	@Override
	public Long getByName(String name) {
		Long roleId = null;
		roleId = jdbcTemplate.queryForObject(FIND_USER_ROLE_ID_BY_NAME_SQL, new Object[] { name }, new RowMapper<Long>() {
			@Override
			public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getLong(1);
			}
		});
		return roleId;
	}

	@Override
	public Collection<UserRole> getAll() {
		return null;
	}

}
