package com.epam.spring.common.dao;

import java.util.List;

import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Auditorium;

public interface AuditoriumDao extends AbstractDomainObjectDao<Auditorium>{

	Long saveAuditoriumVipSeat(Long auditoriumId, Long vipSeat);
	
	List<AirDate> getAllEventAuditoriums(Long eventId);
	
	void updateAuditorium(Auditorium auditorium);
	
	void removeAuditoriumVipSeats(Long auditoriumId);
    
    void saveAuditoriumVipSeats(Long auditoriumId, List<Long> vipSeats);
}
