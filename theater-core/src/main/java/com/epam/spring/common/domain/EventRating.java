package com.epam.spring.common.domain;

/**
 * @author Andrei_Seliazniou
 */
public enum EventRating {

    LOW,

    MID,

    HIGH;

}
