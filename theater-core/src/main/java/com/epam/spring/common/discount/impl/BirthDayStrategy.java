package com.epam.spring.common.discount.impl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.epam.spring.common.discount.DiscountStrategy;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.User;

@Component("birthDayStrategy")
public class BirthDayStrategy implements DiscountStrategy {

	@Value("${birth.day.discount}")
	private byte birthDayDiscount;

	@Override
	public byte getDiscount(User user, Event event, LocalDate airDate, long numberOfTickets) {
		byte discount = 0;
		if (user != null && airDate.minusDays(5).isBefore(user.getBirthDay())
				&& airDate.plusDays(5).isAfter(user.getBirthDay())) {
			discount = birthDayDiscount;
		}
		return discount;
	}

}
