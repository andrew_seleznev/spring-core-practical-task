package com.epam.spring.common.service.impl;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.epam.spring.common.dao.AirDateDao;
import com.epam.spring.common.dao.AuditoriumDao;
import com.epam.spring.common.dao.EventDao;
import com.epam.spring.common.domain.AirDate;
import com.epam.spring.common.domain.Auditorium;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.service.AirDateService;
import com.epam.spring.common.service.BookingService;

@Service("airDateService")
public class AirDateServiceImpl implements AirDateService {
	
	static Logger logger = LogManager.getLogger(AirDateServiceImpl.class);
	@Autowired
	@Qualifier("airDateDao")
	private AirDateDao airDateDao;
	@Autowired
	@Qualifier("auditoriumDao")
	private AuditoriumDao auditoriumDao;
	@Autowired
	private EventDao eventDao;
	@Autowired
	private BookingService bookingService;

	@Override
	public Long save(AirDate object) {
		Long airDateId = airDateDao.save(object);
		return airDateId;
	}

	@Override
	public void remove(AirDate object) {
		airDateDao.remove(object);
	}

	@Override
	public AirDate getById(Long id) {
		AirDate airDate = airDateDao.getById(id);
		airDate.setAuditorium(auditoriumDao.getById(airDate.getAuditorium().getId()));
		airDate.setAvailableSeats(findFreeSeats(airDate));
		return airDate;
	}

	@Override
	public Collection<AirDate> getAll() {
		return null;
	}

	@Override
	public void updateAirDate(AirDate airDate) {
		airDateDao.updateAirDate(airDate);
	}
	
	private List<Long> findFreeSeats(AirDate airDate) {
        Auditorium aud = airDate.getAuditorium();
        Event event = eventDao.getById(airDate.getEventId());
        LocalDateTime time = airDate.getDateTime();
        Set<Ticket> tickets = bookingService.getPurchasedTicketsForEvent(event, time);
        
        List<Long> bookedSeats = tickets.stream().map(t -> t.getSeat()).collect(Collectors.toList());
        List<Long> freeSeats = aud.getAllSeats().stream().filter(seat -> !bookedSeats.contains(seat))
                .collect(Collectors.toList());
        return freeSeats;
    }

}
