package com.epam.spring.common.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.epam.spring.common.dao.BookingDao;
import com.epam.spring.common.domain.Ticket;

@Component("bookingDao")
public class BookingDaoImpl implements BookingDao {

	private static final String SAVE_TICKET = "INSERT INTO TICKET (VISITOR_ID, EVENT_ID, TICKET_DATE, TICKET_SEAT, AIR_DATE_ID) VALUES (?, ?, ?, ?, ?)";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void bookTickets(Set<Ticket> tickets) {
		List<Ticket> list = new ArrayList<>(tickets);
		jdbcTemplate.batchUpdate(SAVE_TICKET, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Ticket ticket = list.get(i);
				ps.setLong(1, ticket.getUser().getId());
				ps.setLong(2, ticket.getEvent().getId());
				ps.setTimestamp(3, Timestamp.valueOf(ticket.getDateTime()));
				ps.setLong(4, ticket.getSeat());
				ps.setLong(5, ticket.getAirDate().getId());
			}

			@Override
			public int getBatchSize() {
				return tickets.size();
			}
		});
	}

}
