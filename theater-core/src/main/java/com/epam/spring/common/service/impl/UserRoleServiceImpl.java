package com.epam.spring.common.service.impl;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.epam.spring.common.dao.UserRoleDao;
import com.epam.spring.common.domain.UserRole;
import com.epam.spring.common.service.UserRoleService;

@Component("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {
	
	static Logger logger = LogManager.getLogger(UserRoleServiceImpl.class);
	@Autowired
	@Qualifier("userRoleDao")
	private UserRoleDao userRoleDao;

	@Override
	public Long save(UserRole object) {
		return null;
	}

	@Override
	public void remove(UserRole object) {
		
	}

	@Override
	public Long getByName(String name) {
		return userRoleDao.getByName(name);
	}

	@Override
	public Collection<UserRole> getAll() {
		return null;
	}

}
