package com.epam.spring.common.discount;

import java.time.LocalDate;

import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.User;

public interface DiscountStrategy {
	
	byte getDiscount(User user, Event event, LocalDate airDate, long numberOfTickets);
	
}
