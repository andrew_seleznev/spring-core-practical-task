package com.epam.spring.common.aspect;

import java.time.LocalDateTime;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.epam.spring.common.domain.Counter;
import com.epam.spring.common.domain.CountingCriteria;
import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.service.CounterService;

@Aspect
@Component("counterAspect")
public class CounterAspect {

	static Logger logger = LogManager.getLogger(CounterAspect.class);

	@Autowired
	@Qualifier("counterService")
	private CounterService counterService;

	@Pointcut("execution(* com.epam.spring.common.service.impl.EventServiceImpl.getByName(..))")
	private void accessedByNameEventsMethod() {
	}

	@AfterReturning(pointcut = "accessedByNameEventsMethod() && args(name)", returning = "retVal")
	public void countAccessedByNameEvents(String name, Event retVal) {
		if (retVal != null) {
			Long eventId = retVal.getId();
			Counter counter = counterService.getByCountingCriteria(
					CountingCriteria.EVENT_ACCESSED_BY_NAME, eventId);
			if (counter == null) {
				Counter object = new Counter();
				object.setCountedObjectId(eventId);
				object.setCountingCriteria(CountingCriteria.EVENT_ACCESSED_BY_NAME);
				object.setCount(0);
				counterService.save(object);
				counter = counterService.getByCountingCriteria(
						CountingCriteria.EVENT_ACCESSED_BY_NAME, eventId);
			}
			counter.setCount(counter.getCount() + 1);
			counterService.updateCounter(counter);
			logger.info(retVal.getName()
					+ " was accessed by name "
					+ counterService.getByCountingCriteria(
							CountingCriteria.EVENT_ACCESSED_BY_NAME, eventId)
							.getCount() + " time(s)");
		}
	}

	@Pointcut("execution(* com.epam.spring.common.service.impl.BookingServiceImpl.getTicketsPrice(..))")
	private void queriedEventPricesMethod() {
	}

	@AfterReturning(pointcut = "queriedEventPricesMethod() && args(event, dateTime, user, seats)")
	public void countQueriedEventPrices(Event event, LocalDateTime dateTime,
			User user, Set<Long> seats) {
		Long eventId = event.getId();
		Counter counter = counterService.getByCountingCriteria(
				CountingCriteria.EVENT_PRICES_QUERIED, eventId);
		if (counter == null) {
			Counter object = new Counter();
			object.setCountedObjectId(eventId);
			object.setCountingCriteria(CountingCriteria.EVENT_PRICES_QUERIED);
			object.setCount(0);
			counterService.save(object);
			counter = counterService.getByCountingCriteria(
					CountingCriteria.EVENT_PRICES_QUERIED, eventId);
		}
		counter.setCount(counter.getCount() + 1);
		counterService.updateCounter(counter);
		logger.info(event.getName()
				+ " prices were queried "
				+ counterService.getByCountingCriteria(
						CountingCriteria.EVENT_PRICES_QUERIED, eventId)
						.getCount() + " time(s)");
	}

	@Pointcut("execution(* com.epam.spring.common.service.impl.BookingServiceImpl.bookTickets(..))")
	private void bookEventTicketsMethod() {
	}

	@AfterReturning(pointcut = "bookEventTicketsMethod() && args(tickets)")
	public void countBookedEventTicket(Set<Ticket> tickets) {
		for (Ticket ticket : tickets) {
			Event event = ticket.getEvent();
			Long eventId = event.getId();
			Counter counter = counterService.getByCountingCriteria(
					CountingCriteria.EVENT_TICKETS_BOOKED, eventId);
			if (counter == null) {
				Counter object = new Counter();
				object.setCountedObjectId(eventId);
				object.setCountingCriteria(CountingCriteria.EVENT_TICKETS_BOOKED);
				object.setCount(0);
				counterService.save(object);
				counter = counterService.getByCountingCriteria(
						CountingCriteria.EVENT_TICKETS_BOOKED, eventId);
			}
			counter.setCount(counter.getCount() + 1);
			counterService.updateCounter(counter);
			logger.info(event.getName()
					+ " tickets were booked "
					+ counterService.getByCountingCriteria(
							CountingCriteria.EVENT_TICKETS_BOOKED, eventId)
							.getCount() + " time(s)");
		}
	}

}
