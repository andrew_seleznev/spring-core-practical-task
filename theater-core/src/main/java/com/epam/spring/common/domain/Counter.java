package com.epam.spring.common.domain;

public class Counter extends DomainObject {
	
	private long countedObjectId;
	private CountingCriteria countingCriteria;
	private long count;
	
	public long getCountedObjectId() {
		return countedObjectId;
	}
	public void setCountedObjectId(long countedObjectId) {
		this.countedObjectId = countedObjectId;
	}
	public CountingCriteria getCountingCriteria() {
		return countingCriteria;
	}
	public void setCountingCriteria(CountingCriteria countingCriteria) {
		this.countingCriteria = countingCriteria;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (count ^ (count >>> 32));
		result = prime * result
				+ (int) (countedObjectId ^ (countedObjectId >>> 32));
		result = prime
				* result
				+ ((countingCriteria == null) ? 0 : countingCriteria.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Counter other = (Counter) obj;
		if (count != other.count)
			return false;
		if (countedObjectId != other.countedObjectId)
			return false;
		if (countingCriteria != other.countingCriteria)
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Counter [countedObjectId=").append(countedObjectId)
				.append(", countingCriteria=").append(countingCriteria)
				.append(", count=").append(count).append("]");
		return builder.toString();
	}

}
