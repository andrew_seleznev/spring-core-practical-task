package com.epam.spring.common.dao;

import java.util.Collection;

import javax.annotation.Nonnull;

import com.epam.spring.common.domain.DomainObject;

/**
 * @author Andrei_Seliazniou
 *
 * @param <T>
 *            DomainObject subclass
 */
public interface AbstractDomainObjectDao<T extends DomainObject> {
	
	/**
     * Saving new object to storage or updating existing one
     * 
     * @param object
     *            Object to save
     * @return saved object with assigned id
     */
    Long save(@Nonnull T object);

    /**
     * Removing object from storage
     * 
     * @param object
     *            Object to remove
     */
    void remove(@Nonnull T object);

    /**
     * Getting object by id from storage
     * 
     * @param id
     *            id of the object
     * @return Found object or <code>null</code>
     */
    T getById(@Nonnull Long id);

    /**
     * Getting all objects from storage
     * 
     * @return collection of objects
     */
    @Nonnull Collection<T> getAll();
    
}
