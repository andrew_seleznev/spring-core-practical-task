package com.epam.spring.common.service;

import com.epam.spring.common.domain.Counter;
import com.epam.spring.common.domain.CountingCriteria;

public interface CounterService extends AbstractDomainObjectService<Counter> {
	
	Counter getByCountingCriteria (CountingCriteria countingCriteria, Long countedObjectId);
	
	void updateCounter (Counter counter);
}
