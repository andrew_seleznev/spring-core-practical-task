package com.epam.spring.common.dao;

import com.epam.spring.common.domain.Counter;
import com.epam.spring.common.domain.CountingCriteria;

public interface CounterDao extends AbstractDomainObjectDao<Counter> {
	
	Counter getByCountingCriteria (CountingCriteria countingCriteria, Long countedObjectId);
	
	void updateCounter (Counter counter);
}
