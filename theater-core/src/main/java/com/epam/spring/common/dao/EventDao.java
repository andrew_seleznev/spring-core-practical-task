package com.epam.spring.common.dao;

import java.time.LocalDateTime;
import java.util.NavigableSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.EventRating;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;

public interface EventDao extends AbstractDomainObjectDao<Event> {
	
	/**
     * Finding event by name
     * 
     * @param name
     *            Name of the event
     * @return found event or <code>null</code>
     */
    @Nullable Event getByName(@Nonnull String name);

    /*
     * Finding all events that air on specified date range
     * 
     * @param from Start date
     * 
     * @param to End date inclusive
     * 
     * @return Set of events
     */
    // public @Nonnull Set<Event> getForDateRange(@Nonnull LocalDate from,
    // @Nonnull LocalDate to);

    /*
     * Return events from 'now' till the the specified date time
     * 
     * @param to End date time inclusive
     * s
     * @return Set of events
     */
    // public @Nonnull Set<Event> getNextEvents(@Nonnull LocalDateTime to);
    @Nullable Long saveEventRating(@Nonnull EventRating eventRating);
    NavigableSet<Ticket> getAllUserTickets(User user);
    
    @Nullable Long saveEventAuditorium(@Nonnull Long eventId, @Nonnull Long auditoriumId, @Nonnull LocalDateTime eventDate);
    void updateEvent(@Nonnull Event event);
}
