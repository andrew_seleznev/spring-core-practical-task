package com.epam.spring.common.dao;

import java.time.LocalDateTime;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.epam.spring.common.domain.Event;
import com.epam.spring.common.domain.Ticket;
import com.epam.spring.common.domain.User;
import com.epam.spring.common.domain.UserRole;

public interface UserDao extends AbstractDomainObjectDao<User> {
	
	/**
     * Finding user by email
     * 
     * @param email
     *            Email of the user
     * @return found user or <code>null</code>
     */
    @Nullable User getUserByEmail(@Nonnull String email);
    
    Set<Ticket> getAllEventTickets(Event event, LocalDateTime dateTime);
    Set<UserRole> getUserRoles(User user);
    
    Long saveUserRole(Long userId, Long roleId);
    
    Set<Ticket> getAllUserTickets(User user);
    
}
